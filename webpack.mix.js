const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .styles([
        'resources/theme/dashboard/plugins/fontawesome-free/css/all.min.css',
        'resources/theme/dashboard/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css',
        'resources/theme/dashboard/plugins/toastr/toastr.min.css',
        'resources/theme/dashboard/css/adminlte.min.css',
        'resources/theme/dashboard/plugin/overlayScrollbars/css/OverlayScrollbars.min.css',
        'resources/theme/dashboard/css/custom.css'
    ], 'public/dashboard/css/vendor.min.css')


    .styles([
        'resources/theme/website/css/bootstrap.min.css',
        'resources/theme/website/css/animate.css',
        'resources/theme/website/css/jquery-ui.min.css',
        'resources/theme/website/css/meanmenu.min.css',
        'resources/theme/website/css/owl.carousel.min.css',
        'resources/theme/website/css/jquery.bxslider.css',
        'resources/theme/website/css/magnific-popup.css',
        'resources/theme/website/css/font-awesome.min.css',
        'resources/theme/website/css/flaticon.css',
        'resources/theme/website/css/style.css',
        'resources/theme/website/css/responsive.css',
    ], 'public/website/css/vendor.min.css')

    .scripts([
        'resources/theme/dashboard/plugins/jquery/jquery.min.js',
        'resources/theme/dashboard/plugins/jquery-ui/jquery-ui.min.js',
        'resources/theme/dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js',
        'resources/theme/dashboard/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
        'resources/theme/dashboard/plugins/toastr/toastr.min.js',
        'resources/theme/dashboard/js/adminlte.min.js',
        'resources/theme/dashboard/js/script.js',
        'resources/theme/custom.js',
    ], 'public/dashboard/js/vendor.min.js')

    .scripts([
        'resources/theme/website/js/jquery-1.12.0.min.js',
        'resources/theme/website/js/tether.min.js',
        'resources/theme/website/js/bootstrap.min.js',
        'resources/theme/website/js/owl.carousel.min.js',
        'resources/theme/website/js/jquery.bxslider.min.js',
        'resources/theme/website/js/isotope.pkgd.min.js',
        'resources/theme/website/js/jquery.magnific-popup.min.js',
        'resources/theme/website/js/jquery.meanmenu.js',
        'resources/theme/website/js/jarallax.min.js',
        'resources/theme/website/js/jquery-ui.min.js',
        'resources/theme/website/js/jquery.downCount.js',
        'resources/theme/website/js/jquery.counterup.min.js',
        'resources/theme/website/js/waypoints.min.js',
        'resources/theme/website/js/jquery.mixitup.min.js',
        'resources/theme/website/js/wow.min.js',
        'resources/theme/website/js/plugins.js',
        'resources/theme/website/js/main.js',
        'resources/theme/custom.js',
    ], 'public/website/js/vendor.min.js')

    .copy([
        'resources/theme/dashboard/plugins/fontawesome-free/webfonts',
    ], 'public/dashboard/webfonts')

    .copy([
        'resources/theme/website/fonts',
    ], 'public/website/fonts')


    .copy([
        'resources/theme/dashboard/img',
    ], 'public/dashboard/img')

    .copy([
        'resources/theme/website/images',
    ], 'public/website/images')

    .copy([
        'resources/theme/website/css/icon',
    ], 'public/website/css/images/icon');


