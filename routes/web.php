<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');

/* All Routed Related to the Dashboard*/
Route::group(['prefix' => '/dash', 'middleware' => ['maintenance', 'permissions', 'localized']], function () {


    Route::get('/', ['as' => 'dashboard', 'uses' => 'Dashboard\HomeController@index']);
    Route::resource('notifications', 'Dashboard\NotificationController');

    Route::resource('institutes', 'Dashboard\InstituteController');
    Route::post('institutes/{id}/edit', 'Dashboard\InstituteController@update');
    Route::get('institutes/status/{type}/{id}', 'Dashboard\InstituteController@change_status')->name('institutes.status');
    Route::get('branch/status/{type}/{id}', 'Dashboard\BranchController@change_status')->name('branches.status');

    Route::resource('institute-manage-dashboard', 'Dashboard\ManageDashboardController');

    Route::resource('banners', 'Dashboard\BannerController');
    Route::resource('services', 'Dashboard\ServiceController');
    Route::resource('albums', 'Dashboard\AlbumController');


});

/* All Routed Related to the WebSite*/
Route::group(['prefix' => '/', 'middleware' => ['maintenance', 'permissions', 'localized']], function () {

    Route::get('', ['as' => 'home', 'uses' => 'Website\HomeController@index']);

});

Route::get('lang/switcher/{type}', ['as' => 'lang.switcher', 'uses' => 'LanguageController@apply']);
