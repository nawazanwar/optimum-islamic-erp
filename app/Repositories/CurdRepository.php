<?php

namespace App\Repositories;

use App\Interfaces\Curd;
use Illuminate\Database\Eloquent\Model;

class CurdRepository implements Curd
{
    // model property on class instances
    protected $model;

    // Constructor to bind model to repo
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all(array $data)
    {
        if (isset($data['field']) && isset($data['keyword'])) {
            $this->model = $this->model->where("{$data['field']}", 'LIKE', "%{$data['keyword']}%");
        }
        return $this->model;
    }

    // create a new record in the database
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    // update record in the database
    public function update(array $data, $id)
    {
        $record = $this->find($id);
        return $record->update($data);
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    //find sinle
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }

    //Create or update
    public function updateOrCreate($filter, $data)
    {
        return $this->model->updateOrCreate($filter, $data);
    }

    // Find by where

    public function exists($key, $value)
    {
        return $this->model->where($key, $value)->exists();
    }

}
