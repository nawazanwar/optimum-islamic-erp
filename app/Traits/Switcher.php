<?php

namespace App\Traits;

use App;
use Illuminate\Support\Facades\DB;

trait Switcher
{

    public function changeCurrentUrl($url, $lang = null)
    {
        $matched_array = array('?lang=en', '&lang=en', '?lang=ur', '&lang=ur');
        foreach ($matched_array as $match_string) {
            if (strpos($url, $match_string) !== false) {
                $url = str_replace($match_string, '', $url);
            }
        }
        if (parse_url($url, PHP_URL_QUERY)) {
            $url = $url . "&lang=" . $lang;
        } else {
            $url = $url . "?lang=" . $lang;
        }
        return $url;
    }
}
