<?php

namespace App\Http\Controllers;
use App;
use App\Traits\Switcher;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    use Switcher;

    public function apply(Request $request, $locale)
    {
        app()->setLocale($locale);
        session()->put('locale', $locale);
        return redirect($this->changeCurrentUrl(url()->previous(), $locale));
    }
}
