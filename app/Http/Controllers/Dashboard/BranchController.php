<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Repositories\CurdRepository;
use App\Traits\General;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    protected $model;

    use General;

    public function __construct(Branch $branch)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($branch);
    }

    public function change_status($type, $id)
    {
        $this->model = $this->model->find($id);
        if ($this->model) {
            switch ($type) {
                case "active":
                    $this->model->active = false;
                    break;
                case  "in_active":
                    $this->model->active = true;
                    break;
            }
            $this->model->save();
            return response()->json([
                'type' => $type
            ]);
        }
    }
}
