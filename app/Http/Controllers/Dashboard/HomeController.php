<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
        $pageTitle = __('dashboard.welcome_back');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];
        return view('dashboard.index', $viewParams);
    }
}
