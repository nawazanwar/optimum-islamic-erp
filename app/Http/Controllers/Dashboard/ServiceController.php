<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Service;
use App\Repositories\CurdRepository;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;

class ServiceController extends Controller
{
    protected $model;

    use General;

    public function __construct(Service $service)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($service);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('read', Service::class);
        $pageTitle = __('service.title');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $request->query('field', null),
            'keyword' => $request->query('keyword', null),
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $this->model->all($request->all())->get()
        ];
        return view('dashboard.services.index', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('edit', Service::class);
        $input = $request->all();
        $length = count($input['name_en']);

        for ($i = 0; $i < $length; $i++) {

            $name_en = $input['name_en'][$i];
            $name_ur = $input['name_ur'][$i];
            $slug = $input['slug'][$i];
            $active = $input['active'][$i];
            $data = [
                'name_en' => $name_en,
                'name_ur' => $name_ur,
                'slug' => $slug,
                'active' => $active,
            ];
            //save or update
            $this->model->updateOrCreate([
                'name_en' => $name_en,
                'slug' => $slug
            ], $data);
            return redirect()->route('services.index');
        }
    }
}
