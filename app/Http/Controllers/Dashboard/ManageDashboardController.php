<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Institute;
use App\Models\Service;
use App\Repositories\CurdRepository;
use App\Traits\General;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ManageDashboardController extends Controller
{
    protected $model;

    use General;

    public function __construct(Institute $institute)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($institute);
    }

    public function index(Request $request)
    {
        $this->model = $this->model->find($request->get('pId'));
        $this->authorize('read', Institute::class);
        $pageTitle = __('institute.view_dashboard_of');
        if (app()->getLocale() == 'en') {
            $pageTitle = $pageTitle . "  " . $this->model->name_en;
        } else {
            $pageTitle = $this->model->name_ur . " " . $pageTitle;
        }
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $request->query('field', null),
            'keyword' => $request->query('keyword', null),
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $this->model
        ];
        return view('dashboard.institutes.manage', $viewParams);
    }

    public function store(Request $request)
    {
        $this->model = $this->model->find($request->get('pId'));
        if ($request->get('services')) {

            $services = $request->get('services', null);
            DB::table('institute_service')
                ->whereInstituteId($this->model->id)
                ->update([
                    'is_sync' => false,
                ]);

            foreach ($services as $service) {

                DB::table('institute_service')
                    ->whereInstituteId($this->model->id)
                    ->whereServiceId($service)
                    ->update([
                        'is_sync' => true,
                        'updated_at' => Carbon::now()
                    ]);
            }
        }

        return \response()->json([
            'status' => 200,
            'services' => $request->get('services', array()),
            'domain' => $this->model->credentials
        ]);
    }

}
