<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Credential;
use App\Models\Gallery;
use App\Models\Service;
use App\Models\Owner;
use App\Models\Institute;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Repositories\CurdRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class AlbumController extends Controller
{
    protected $model;

    use General;

    public function __construct(Album $album)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($album);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('read', Album::class);
        $pageTitle = __('album.title');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $request->query('field', null),
            'keyword' => $request->query('keyword', null),
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $this->model->all($request->all())->paginate(12)
        ];
        return view('dashboard.albums.index', $viewParams);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Album::class);

        $pageTitle = __('album.create');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
        ];

        return view('dashboard.albums.create', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Album::class);
        /*applied validations*/
        $request->validate([
            'name' => 'unique:albums,name',
            'name_ur' => 'unique:albums,name_ur'
        ]);
        $data = $request->only($this->model->getModel()->fillable);
        // Save the Detail of the Institute
        $this->model = $this->model->create($data);
        if ($this->model) {

            if ($request->file('album_avatar')) {

                //General trait function
                $this->makeMultipleDirectories('albums', ['avatars']);
                $avatar = $request->file('album_avatar');
                $avatar_name = sha1('img' . $this->model->id . time()) . '.' . $avatar->getClientOriginalExtension();
                $avatar = Image::make($avatar);
                $avatar->save(public_path('uploads/albums/avatars/') . $avatar_name);
                $this->model->avatar = $avatar_name;
                $this->model->save();

            }

            /* Ready to save the gallery images*/

            if ($request->file('galleries')) {
                $galleries = $request->file('galleries');
                $this->makeMultipleDirectories('albums', ['galleries']);
                $count = count($galleries);
                for ($i = 0; $i < $count; $i++) {

                    $gallery = $galleries[$i];

                    if ($gallery) {
                        $extension = $gallery->getClientOriginalExtension();
                        $gallery_avatar_name = sha1('img' . $this->model->id . time()) . '.' . $extension;
                        $gallery_avatar = Image::make($gallery);
                        $gallery_avatar->backup();
                        $gallery_avatar->save(public_path('uploads/albums/galleries/') . $gallery_avatar_name);
                        $this->model->galleries()->save(new Gallery([
                            'avatar' => $gallery_avatar_name
                        ]));
                    }
                }
            }

            return response()->json([
                'status' => 200,
                'message' => __('message.record_created_success_message')
            ]);

        } else {

            return response()->json([
                'status' => 404,
                'message' => __('message.record_created_failed_message')
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     */
    public function edit($id)
    {
        $this->authorize('edit', Institute::class);
        $this->model = $this->model->find($id);

        $pageTitle = __('btn.edit') . " " . $this->model->name;
        $breadcrumbs = [['text' => __('system.edit_venue')]];
        $viewParams = [
            'model' => $this->model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];

        return view('dashboard.institutes.edit', $viewParams);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return void
     * @throws AuthorizationException
     */
    public function update(Request $request, $id)
    {
        $this->authorize('edit', Institute::class);
        $request->validate([
            'name' => 'required|max:255|unique:institutes,name,' . $id,
            'name_ur' => 'required|max:255|unique:institutes,name_ur,' . $id,
        ]);
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        $this->model = $this->model->find($id);
        if ($this->model) {
            if ($request->file('avatar')) {
                $existing_image = public_path('uploads/institutes/') . $this->model->avatar;
                if (file_exists($existing_image)) {
                    unlink($existing_image);
                }
                $imageName = sha1('img' . $this->model->id . time()) . '.' . $request->file('avatar')->getClientOriginalExtension();
                $image = Image::make($request->file('avatar'));
                $image->backup();
                $image->save(public_path('uploads/institutes/') . $imageName);
                $this->model->avatar = $imageName;
                $this->model->save();
            }

            // Ready to save the owners
            if ($request->has('owners')) {
                $this->makeMultipleDirectories('institutes', ['owners']);
                $owners = $request->get('owners');
                $count = count($owners['name']);
                for ($i = 0; $i < $count; $i++) {

                    $existing_id = isset($owners['id'][$i]) ? $owners['id'][$i] : '';

                    if (!empty($existing_id)) {

                        $owner = Owner::find($existing_id);
                        $owner->name = $owners['name'][$i];
                        $owner->name_ur = $owners['name_ur'][$i];
                        $owner->save();

                    } else {

                        $owner = $this->model->owners()->save(
                            new Owner(
                                [
                                    'name' => $owners['name'][$i],
                                    'name_ur' => $owners['name_ur'][$i]
                                ]));
                    }

                    // ready to save & update the avatar of each owner

                    $owner_file = isset($request->file('owners')['avatar'][$i]) ? $request->file('owners')['avatar'][$i] : '';

                    if (!empty($owner_file)) {

                        $existing_image = public_path('uploads/institutes/owners/') . $owner->avatar;
                        if (!is_dir($existing_image) && file_exists($existing_image)) {
                            unlink($existing_image);
                        }
                        $extension = $owner_file->getClientOriginalExtension();
                        $owner_avatar_name = sha1('img' . $this->model->id . $owner->id . time()) . '.' . $extension;
                        $owner_image = Image::make($owner_file);
                        $owner_image->backup();
                        $owner_image->save(public_path('uploads/institutes/owners/') . $owner_avatar_name);
                        $owner->avatar = $owner_avatar_name;
                        $owner->save();

                    }
                }
            }

            return response()->json([
                'status' => 200,
                'message' => __($this->model->name . " " . __('message.record_updated_success_message'))
            ]);

        } else {

            // show error in case of error

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy($id)
    {
        $this->authorize('delete', Institute::class);
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('institutes.index')->with('errorMessage', $this->model->name . " " . __('message.deleted_successfully'));
        }
    }
}
