<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Notification;
use App\Repositories\CurdRepository;
use App\Traits\General;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    protected $model;

    use General;

    public function __construct(Notification $notification)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($notification);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('read', Notification::class);
        $pageTitle = __('notification.title');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $request->query('field', null),
            'keyword' => $request->query('keyword', null),
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => auth()->user()->unreadNotifications()->paginate(10)
        ];
        return view('dashboard.notifications.index', $viewParams);
    }

}
