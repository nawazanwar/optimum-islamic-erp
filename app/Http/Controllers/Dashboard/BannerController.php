<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Repositories\CurdRepository;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;

class BannerController extends Controller
{
    protected $model;

    use General;

    public function __construct(Banner $banner)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($banner);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('read', Banner::class);
        $pageTitle = __('banner.title');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $request->query('field', null),
            'keyword' => $request->query('keyword', null),
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $this->model->all($request->all())->get()
        ];
        return view('dashboard.banners.index', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('edit', Banner::class);
        $input = $request->all();
        $length = count($input['heading']);
        $banner = null;
        for ($i = 0; $i < $length; $i++) {

            $heading = $input['heading'][$i];
            $heading_ur = $input['heading_ur'][$i];
            $active = isset($input['active'][$i]) ? 1 : 0;
            $data = [
                'heading' => $heading,
                'heading_ur' => $heading_ur,
                'active' => $active
            ];
            //save or update
            if (isset($input['id'][$i]) && !empty($input['id'][$i])) {

                $this->model->update($data, $input['id'][$i]);
                $banner = $this->model->find($input['id'][$i]);

                if (isset($request->file('image')[$i])) {
                    $removed_image = 'uploads/banners/images/' . $banner->image;
                    if (file_exists(public_path() . '/' . $removed_image) && !is_dir($removed_image)) {
                        unlink($removed_image);
                    }
                }

                if (isset($request->file('avatar')[$i])) {
                    $removed_avatar = 'uploads/banners/avatars/' . $banner->avatar;
                    if (file_exists(public_path() . '/' . $removed_avatar) && !is_dir($removed_avatar)) {
                        unlink($removed_avatar);
                    }
                }

            } else {

                $banner = $this->model->create($data);

            }

            if (isset($request->file('image')[$i])) {
                $image = $request->file('image')[$i];
                $this->makeDirectory('banners/images');
                $extension = $image->getClientOriginalExtension();
                $image_name = sha1('img' . $banner->id . time()) . '.' . $extension;
                $image = Image::make($image);
                $image->backup();
                $image->save(public_path('uploads/banners/images/') . $image_name);
                $banner->image = $image_name;
                $banner->save();
            }

            if (isset($request->file('avatar')[$i])) {
                $avatar = $request->file('avatar')[$i];
                $this->makeDirectory('banners/avatars');
                $extension = $avatar->getClientOriginalExtension();
                $avatar_name = sha1('img' . $banner->id . time()) . '.' . $extension;
                $avatar = Image::make($avatar);
                $avatar->backup();
                $avatar->save(public_path('uploads/banners/avatars/') . $avatar_name);
                $banner->avatar = $avatar_name;
                $banner->save();

            }

        }

        return redirect()->route('banners.index');
    }
}
