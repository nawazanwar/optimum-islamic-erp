<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Credential;
use App\Models\Service;
use App\Models\Owner;
use App\Models\Institute;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Repositories\CurdRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class InstituteController extends Controller
{
    protected $model;

    use General;

    public function __construct(Institute $institute)
    {
        $this->middleware('auth');
        // set the model
        $this->model = new CurdRepository($institute);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('read', Institute::class);
        $pageTitle = __('institute.title');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $request->query('field', null),
            'keyword' => $request->query('keyword', null),
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $this->model->all($request->all())->paginate(12)
        ];
        return view('dashboard.institutes.index', $viewParams);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $id
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $this->model = $this->model->find($id);
        $this->authorize('read', Institute::class);
        $pageTitle = __('institute.view_detail_of');
        if (app()->getLocale() == 'en') {
            $pageTitle = $pageTitle . "  " . $this->model->name_en;
        } else {
            $pageTitle = $this->model->name_ur . " " . $pageTitle;
        }
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $request->query('field', null),
            'keyword' => $request->query('keyword', null),
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $this->model
        ];
        return view('dashboard.institutes.show', $viewParams);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Institute::class);

        $pageTitle = __('institute.create');
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'services' => Service::all()
        ];
        return view('dashboard.institutes.create', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Institute::class);
        $institute_data = $request->get('institute');
        // if name is already exits
        if ($this->model->exists('name_en', $institute_data['name_en']) == 1) {

            return response()->json([
                'status' => 409,
                'message' => __('institute.message.name_already_exists')
            ]);

        } else {

            // First of all save the information of Institute
            $this->model = $this->model->create($institute_data);
            if ($this->model) {

                $this->model->active = false;
                $this->model->slug = Str::slug($this->model->name_en, '-');
                $this->model->save();

                // Ready to save the avatar for the Institute

                if ($request->file('avatar')) {

                    $avatar = $request->file('avatar');
                    //General trait function
                    $this->makeDirectory('institutes');
                    $imageName = sha1('img' . $this->model->id . time()) . '.' . $avatar->getClientOriginalExtension();
                    $image = Image::make($avatar);
                    $image->resize('250', '250');
                    $image->save(public_path('uploads/institutes/') . $imageName);
                    $this->model->avatar = $imageName;
                    $this->model->save();

                }

                // Ready to save the information of of the Owners

                if ($request->has('owners')) {
                    $this->makeMultipleDirectories('institutes', ['owners']);
                    $owners = $request->get('owners');
                    $owner_count = count($owners['name_ur']);
                    for ($i = 0; $i < $owner_count; $i++) {
                        $this->model->owners()->save(
                            $owner = new Owner(
                                [
                                    'name_en' => $owners['name_en'][$i],
                                    'name_ur' => $owners['name_ur'][$i],
                                    'phone' => $owners['phone'][$i],
                                    'cnic' => $owners['cnic'][$i],
                                    'active' => false
                                ]));
                        // ready to save & update the avatar of each owner

                        $owner_file = $request->file('owners')['avatar'][$i];

                        if ($owner_file) {
                            $extension = $owner_file->getClientOriginalExtension();
                            $owner_avatar_name = sha1('img' . $this->model->id . $owner->id . time()) . '.' . $extension;
                            $owner_image = Image::make($owner_file);
                            $owner_image->resize('250', '250');
                            $owner_image->save(public_path('uploads/institutes/owners/') . $owner_avatar_name);
                            $owner->avatar = $owner_avatar_name;
                            $owner->save();

                        }
                    }
                }


                // Ready to save the Information of the branches

                if ($request->has('branches')) {
                    $this->makeMultipleDirectories('institutes', ['branches']);
                    $branches = $request->get('branches');
                    $branch_count = count($branches['name_ur']);
                    for ($i = 0; $i < $branch_count; $i++) {
                        $this->model->branches()->save(
                            $branch = new Branch(
                                [
                                    'name_en' => $branches['name_en'][$i],
                                    'name_ur' => $branches['name_ur'][$i],
                                    'location_en' => $branches['location_en'][$i],
                                    'location_ur' => $branches['location_ur'][$i],
                                    'slug' => Str::slug($branches['name_en'][$i], '-'),
                                    'active' => false
                                ]));
                        // ready to save & update the avatar of each owner

                        $branch_file = $request->file('branches')['avatar'][$i];

                        if ($branch_file) {

                            $extension = $branch_file->getClientOriginalExtension();
                            $branch_avatar_name = sha1('img' . $this->model->id . $branch->id . time()) . '.' . $extension;
                            $branch_image = Image::make($branch_file);
                            $branch_image->resize('250', '250');
                            $branch_image->save(public_path('uploads/institutes/branches/') . $branch_avatar_name);
                            $branch->avatar = $branch_avatar_name;
                            $branch->save();

                        }
                    }
                }

                // Ready to save the service of the institute

                if ($request->has('services')) {

                    $services = $request->get('services', array());
                    if (count($services) > 0) {
                        foreach ($services as $sId) {
                            DB::table('institute_service')->insert([
                                'institute_id' => $this->model->id,
                                'service_id' => $sId
                            ]);
                        }
                    }
                }

                // ready to save the credentials

                $this->model->credentials()->save(new Credential([
                    'hosting_name' => $request->get('hosting_name', null),
                    'hosting_user' => $request->get('hosting_user', null),
                    'hosting_pwd' => $request->get('hosting_pwd', null),
                    'domain_name' => $request->get('domain_name', null),
                    'database_name' => $request->get('database_name', null),
                    'database_user' => $request->get('database_user', null),
                    'database_pwd' => $request->get('database_pwd', null)
                ]));

            }

            return response()->json([
                'status' => 200,
                'message' => __('message.record_created_success_message')
            ]);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     */
    public
    function edit($id)
    {
        $this->authorize('edit', Institute::class);
        $this->model = $this->model->find($id);

        $pageTitle = __('btn.edit') . " " . $this->model->name;
        $breadcrumbs = [['text' => __('system.edit_venue')]];
        $viewParams = [
            'model' => $this->model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle
        ];

        return view('dashboard.institutes.edit', $viewParams);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return void
     * @throws AuthorizationException
     */
    public
    function update(Request $request, $id)
    {
        $this->authorize('edit', Institute::class);
        $request->validate([
            'name' => 'required|max:255|unique:institutes,name,' . $id,
            'name_ur' => 'required|max:255|unique:institutes,name_ur,' . $id,
        ]);
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        $this->model = $this->model->find($id);
        if ($this->model) {
            if ($request->file('avatar')) {
                $existing_image = public_path('uploads/institutes/') . $this->model->avatar;
                if (file_exists($existing_image)) {
                    unlink($existing_image);
                }
                $imageName = sha1('img' . $this->model->id . time()) . '.' . $request->file('avatar')->getClientOriginalExtension();
                $image = Image::make($request->file('avatar'));
                $image->backup();
                $image->save(public_path('uploads/institutes/') . $imageName);
                $this->model->avatar = $imageName;
                $this->model->save();
            }

            // Ready to save the owners
            if ($request->has('owners')) {
                $this->makeMultipleDirectories('institutes', ['owners']);
                $owners = $request->get('owners');
                $count = count($owners['name']);
                for ($i = 0; $i < $count; $i++) {

                    $existing_id = isset($owners['id'][$i]) ? $owners['id'][$i] : '';

                    if (!empty($existing_id)) {

                        $owner = Owner::find($existing_id);
                        $owner->name = $owners['name'][$i];
                        $owner->name_ur = $owners['name_ur'][$i];
                        $owner->save();

                    } else {

                        $owner = $this->model->owners()->save(
                            new Owner(
                                [
                                    'name' => $owners['name'][$i],
                                    'name_ur' => $owners['name_ur'][$i]
                                ]));
                    }

                    // ready to save & update the avatar of each owner

                    $owner_file = isset($request->file('owners')['avatar'][$i]) ? $request->file('owners')['avatar'][$i] : '';

                    if (!empty($owner_file)) {

                        $existing_image = public_path('uploads/institutes/owners/') . $owner->avatar;
                        if (!is_dir($existing_image) && file_exists($existing_image)) {
                            unlink($existing_image);
                        }
                        $extension = $owner_file->getClientOriginalExtension();
                        $owner_avatar_name = sha1('img' . $this->model->id . $owner->id . time()) . '.' . $extension;
                        $owner_image = Image::make($owner_file);
                        $owner_image->backup();
                        $owner_image->save(public_path('uploads/institutes/owners/') . $owner_avatar_name);
                        $owner->avatar = $owner_avatar_name;
                        $owner->save();

                    }
                }
            }

            return response()->json([
                'status' => 200,
                'message' => __($this->model->name . " " . __('message.record_updated_success_message'))
            ]);

        } else {

            // show error in case of error

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy($id)
    {
        $this->authorize('delete', Institute::class);
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('institutes.index')->with('errorMessage', $this->model->name . " " . __('message.deleted_successfully'));
        }
    }

    public function change_status($type, $id)
    {
        $this->model = $this->model->find($id);
        if ($this->model) {
            switch ($type) {
                case "active":
                    $this->model->active = false;
                    break;
                case  "in_active":
                    $this->model->active = true;
                    break;
            }
            $this->model->save();
            return response()->json([
                'type' => $type
            ]);
        }
    }


    public function manage_user_dashboard(Request $request, $id)
    {
        $this->model = $this->model->find($id);
        $this->authorize('read', Institute::class);
        $pageTitle = __('institute.view_dashboard_of');
        if (app()->getLocale() == 'en') {
            $pageTitle = $pageTitle . "  " . $this->model->name_en;
        } else {
            $pageTitle = $this->model->name_ur . " " . $pageTitle;
        }
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'field' => $request->query('field', null),
            'keyword' => $request->query('keyword', null),
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'data' => $this->model
        ];
        return view('dashboard.institutes.manage', $viewParams);
    }

}
