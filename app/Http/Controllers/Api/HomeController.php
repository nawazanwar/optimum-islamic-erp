<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Institute;
use App\Models\Notification;
use App\Models\User;
use App\Notifications\ActiveDeActiveNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function check_status(Request $request, $slug)
    {

        if (Institute::whereSlug($slug)->exists()) {

            if (Institute::whereSlug($slug)->whereActive(true)->exists()) {

                return response()->json([
                    'status' => "true",
                    'slug' => $slug
                ]);

            } else {

                return response()->json([
                    'status' => "false",
                    'slug' => $slug
                ]);

            }

        } else {

            return response()->json([
                'status' => "false",
                'slug' => $slug
            ]);

        }
    }

    public function save_active_de_active_notifications(Request $request, $slug)
    {
        if (Institute::whereSlug($slug)->exists()) {

            $users = DB::table('users as u')
                ->join('role_user as ru', 'ru.user_id', '=', 'u.id')
                ->join('permission_role as pr', 'pr.role_id', '=', 'ru.role_id')
                ->join('permissions as p', 'pr.permission_id', '=', 'p.id')
                ->where('p.name', 'read_notification')->select('u.*');

            if ($users->count() > 0) {
                foreach ($users->get() as $user) {
                    $toUser = User::find($user->id);
                    $details = [
                        'message' => $request->input('message', null),
                        'sender' => Institute::whereSlug($slug)->value('id'),
                    ];
                    $toUser->notify(new ActiveDeActiveNotification($details));
                }

                return response()->json([
                    'status' => 200
                ]);
            } else {
                return response()->json([
                    'status' => 404
                ]);
            }
        } else {
            return response()->json([
                'status' => 404
            ]);
        }
    }
}
