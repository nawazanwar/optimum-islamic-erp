<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $pageTitle = __('website.welcome_back');
        $viewParams = [
            'pageTitle' => $pageTitle
        ];
        return view('website.index', $viewParams);
    }
}
