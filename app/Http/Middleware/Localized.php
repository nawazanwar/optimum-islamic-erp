<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use App;

class Localized
{
    public function handle($request, Closure $next)
    {
        $language_changer = array('ur', 'en');
        if ($request->has('lang')) {
            $lang = $request->get('lang');
            $request->session()->forget('locale');
            if (in_array($lang, $language_changer)) {
                app()->setLocale($lang);
                session()->put('locale', $lang);
            } else {
                app()->setLocale('ur');
                session()->put('locale', 'ur');
            }
        } else {
            $request->session()->forget('locale');
            session()->put('locale', app()->getLocale());
            app()->setLocale(app()->getLocale());
        }
        return $next($request);
    }
}
