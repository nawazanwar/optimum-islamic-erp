<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model implements Permissions
{
    use SoftDeletes;
    protected $fillable = ['name', 'name_ur', 'slug', 'active'];

    public function avatar()
    {
        $avatar = 'uploads/albums/avatars/' . $this->avatar;
        if (file_exists(public_path() . "/" . $avatar) && !is_dir($avatar)) {
            return asset($avatar);
        } else {
            return asset('defaults/no_preview.jpg');
        }
    }

    public function galleries()
    {
        return $this->hasMany(Gallery::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_album');
                    break;
                case 'create':
                case 'store':
                    return array('create_album');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_album');
                    break;
                case 'delete':
                    return array('delete_album');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_album',
            'create_album',
            'edit_album',
            'delete_album',
        );
    }
}
