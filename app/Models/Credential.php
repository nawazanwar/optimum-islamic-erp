<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Credential extends Model
{
    protected $fillable = [
        'hosting_name',
        'hosting_user',
        'hosting_pwd',
        'domain_name',
        'database_name',
        'database_user',
        'database_pwd'
    ];
    protected $table = 'credentials';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function institute()
    {
        return $this->belongsTo(Institute::class);
    }
}
