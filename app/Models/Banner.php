<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model implements Permissions
{
    use SoftDeletes;

    protected $fillable = ['image', 'avatar', 'heading', 'heading_ur', 'active'];
    protected $table = 'banners';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function image()
    {
        $image = 'uploads/banners/images/' . $this->image;
        if (file_exists(public_path() . "/" . $image) && !is_dir($image)) {
            return asset($image);
        } else {
            return asset('defaults/no_preview.jpg');
        }
    }

    public function avatar()
    {
        $avatar = 'uploads/banners/avatars/' . $this->avatar;
        if (file_exists(public_path() . "/" . $avatar) && !is_dir($avatar)) {
            return asset($avatar);
        } else {
            return asset('defaults/no_preview.jpg');
        }
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_banner');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_banner');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_banner',
            'edit_banner',
        );
    }
}
