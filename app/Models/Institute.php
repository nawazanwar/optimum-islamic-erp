<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Institute extends Model implements Permissions
{
    use SoftDeletes;

    protected $fillable = ['name_en', 'name_ur', 'location_en', 'location_ur'];
    protected $table = 'institutes';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getAvatar()
    {
        $avatar = 'uploads/institutes/' . $this->avatar;
        if (file_exists(public_path() . "/" . $avatar) && !is_dir(public_path() . "/" . $avatar)) {
            return asset($avatar);
        } else {
            return asset('defaults/no_preview.jpg');
        }
    }

    public function owners()
    {
        return $this->hasMany(Owner::class);
    }

    public function branches()
    {
        return $this->hasMany(Branch::class);
    }

    public function services()
    {
        return $this->belongsToMany(Service::class, 'institute_service')->withPivot('is_sync');
    }

    public function credentials()
    {
        return $this->hasMany(Credential::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_institute');
                    break;
                case 'create':
                case 'store':
                    return array('create_institute');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_institute');
                    break;
                case 'delete':
                    return array('delete_institute');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_institute',
            'create_institute',
            'edit_institute',
            'delete_institute',
        );
    }
}
