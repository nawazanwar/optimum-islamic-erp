<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends Model
{

    use SoftDeletes;
    protected $fillable = ['name_en', 'name_ur', 'cnic', 'phone', 'institute_id'];
    protected $table = 'owners';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function avatar()
    {
        $avatar = 'uploads/institutes/owners/' . $this->avatar;
        if (file_exists(public_path() . "/" . $avatar)) {
            return asset($avatar);
        } else {
            return asset('defaults/no_preview.jpg');
        }
    }

    public function institute()
    {
        return $this->belongsTo(Institute::class);
    }
}
