<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
    use SoftDeletes;
    protected $fillable = ['avatar'];
    protected $table = 'galleries';

    public function avatar()
    {
        $avatar = 'uploads/albums/galleries/' . $this->avatar;
        if (file_exists(public_path() . "/" . $avatar) && !is_dir($avatar)) {
            return asset($avatar);
        } else {
            return asset('defaults/no_preview.jpg');
        }
    }

    public function album()
    {
        return $this->belongsTo(Album::class);
    }
}
