<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Service extends Model implements Permissions
{
    protected $fillable = ['name_en', 'name_ur', 'active', 'slug'];
    protected $table = 'services';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function institutes()
    {
        return $this->belongsToMany(Institute::class, 'institute_service');
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {

            switch ($route) {
                case 'read':
                    return array('read_service');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_service');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'read_service',
            'edit_service',
        );
    }
}
