<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    use SoftDeletes;

    protected $fillable = ['name_en', 'name_ur', 'location_en', 'location_ur'];
    protected $table = 'branches';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function institute()
    {
        return $this->belongsTo(Institute::class);
    }
}
