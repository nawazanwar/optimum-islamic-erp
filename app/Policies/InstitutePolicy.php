<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class InstitutePolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_institute');
    }

    public function create(User $user)
    {
        return $user->ability('create_institute');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_institute');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_institute');
    }
}
