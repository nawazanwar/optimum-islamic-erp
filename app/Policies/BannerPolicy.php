<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class BannerPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_banner');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_banner');
    }
}
