<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_user');
    }

    public function create(User $user)
    {
        return $user->ability('create_user');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_user');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_user');
    }
}
