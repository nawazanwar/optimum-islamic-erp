<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class ProgramPolicy
{
    use HandlesAuthorization;

    public function read(User $user)
    {
        return $user->ability('read_program');
    }

    public function create(User $user)
    {
        return $user->ability('create_program');
    }

    public function edit(User $user)
    {
        return $user->ability('edit_program');
    }

    public function delete(User $user)
    {
        return $user->ability('delete_program');
    }
}
