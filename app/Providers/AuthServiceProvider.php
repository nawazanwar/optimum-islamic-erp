<?php

namespace App\Providers;

use App\Models\Album;
use App\Models\Banner;
use App\Models\Institute;
use App\Models\Notification;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Service;
use App\Models\Setting;
use App\Policies\AlbumPolicy;
use App\Policies\BannerPolicy;
use App\Policies\InstitutePolicy;
use App\Policies\NotificationPolicy;
use App\Policies\PermissionPolicy;
use App\Policies\RolePolicy;
use App\Policies\ServicePolicy;
use App\Policies\SettingPolicy;
use App\Policies\UserPolicy;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Notification::class => NotificationPolicy::class,
        Setting::class => SettingPolicy::class,
        Permission::class => PermissionPolicy::class,
        Role::class => RolePolicy::class,
        User::class => UserPolicy::class,
        Institute::class => InstitutePolicy::class,
        Banner::class => BannerPolicy::class,
        Service::class => ServicePolicy::class,
        Album::class => AlbumPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
