<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;
use Carbon\Carbon;

class SettingsSeeder extends Seeder
{
    public function run()
    {
        DB::table('settings')->delete();
        $data = [
            [
                'name' => 'maintenance',
                'value' => 0,
                'type' => 'checkbox',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'logo',
                'value' => '',
                'type' => 'file',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'favicon',
                'value' => '',
                'type' => 'file',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'site_name',
                'value' => 'Masjid',
                'type' => 'text',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]

        ];
        DB::table('settings')->insert($data);
    }
}
