<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Album;
use Carbon\Carbon;

class AlbumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'slug' => 'ramzan-shareef',
                'name' => 'Ramzan Shareef',
                'name_ur' => 'رمضان شریف',
                'active' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'slug' => 'eid-ul-fitter',
                'name' => 'Eid Ul Fitter',
                'name_ur' => 'عید الفطر',
                'active' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'slug' => 'eid-ul-azha',
                'name' => 'Eid Ul Azha',
                'name_ur' => 'عید الاضحی',
                'active' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'slug' => '12-rabi-al-awwal',
                'name' => '12 Rabi Al-Awwal',
                'name_ur' => 'عیدمیلادالنبی صلی اللہ علیہ وسلم',
                'active' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ];

        DB::table('albums')->delete();
        foreach ($data as $d) {
            DB::table('albums')->insert($d);
        }
    }
}
