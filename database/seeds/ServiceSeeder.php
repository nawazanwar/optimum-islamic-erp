<?php

use Illuminate\Database\Seeder;
use App\Models\Service;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name_en' => 'Student Management System',
                'name_ur' => 'طلباء کے انتظام کا نظام',
                'slug' => Str::slug('Student Management System', '-'),
                'active' => true,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name_en' => 'Hostel Management System',
                'name_ur' => 'ہاسٹل مینجمنٹ سسٹم',
                'slug' => Str::slug('Hostel Management System', '-'),
                'active' => true,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name_en' => 'Library Management System',
                'name_ur' => 'لائبریری مینجمنٹ سسٹم',
                'slug' => Str::slug('Library Management System', '-'),
                'active' => true,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name_en' => 'Inventory Management System',
                'name_ur' => 'انوینٹری مینجمنٹ سسٹم',
                'slug' => Str::slug('Inventory Management System', '-'),
                'active' => true,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name_en' => 'Payroll Generation System',
                'name_ur' => 'پے رول جنریشن سسٹم',
                'slug' => Str::slug('Payroll Generation System', '-'),
                'active' => true,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name_en' => 'Account Management System',
                'name_ur' => 'اکاؤنٹ مینجمنٹ سسٹم',
                'slug' => Str::slug('Account Management System', '-'),
                'active' => true,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name_en' => 'Raseed Generation System',
                'name_ur' => 'جلدی نسل کا نظام',
                'slug' => Str::slug('Raseed Generation System', '-'),
                'active' => true,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name_en' => 'Occasional Funds Management System',
                'name_ur' => 'کبھی کبھار فنڈز مینجمنٹ سسٹم',
                'slug' => Str::slug('Occasional Funds Management System', '-'),
                'active' => true,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name_en' => 'Employee Management System',
                'name_ur' => 'ملازمین کے انتظام کا نظام',
                'slug' => Str::slug('Employee Management System', '-'),
                'active' => true,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name_en' => 'Time Table Management System',
                'name_ur' => 'ٹائم ٹیبل مینجمنٹ سسٹم',
                'slug' => Str::slug('Time Table Management System', '-'),
                'active' => true,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name_en' => 'E-Learning Management System',
                'name_ur' => 'ای لرننگ مینجمنٹ سسٹم',
                'slug' => Str::slug('E-Learning Management System', '-'),
                'active' => true,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]
        ];
        DB::table('services')->delete();
        DB::table('services')->insert($data);
    }
}
