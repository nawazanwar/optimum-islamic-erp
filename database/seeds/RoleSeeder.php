<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        $roles = [
            [
                'name' => 'admin',
                'label' => 'Admin',
                'label_ur' => ''
            ],
            [
                'name' => 'teacher',
                'label' => 'Teacher',
                'label_ur' => ''
            ],
            [
                'name' => 'student',
                'label' => 'Student',
                'label_ur' => ''
            ]
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }
    }
}
