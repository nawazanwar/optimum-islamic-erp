<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use App\Models\Banner;

use Carbon\Carbon;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'heading' => 'God helps those who help themselves',
                'heading_ur' => 'خدا اپنی مدد کرنے والوں کی مدد کرتا ہے',
                'active' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'heading' => 'In the Name of Allah, the Most Beneficent, the Most Merciful',
                'heading_ur' => 'اللہ کے نام سے جو بہت ہی مہربان اور رحم کرنے والا ہے',
                'active' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];

        DB::table('banners')->truncate();
        foreach ($data as $d) {
            DB::table('banners')->insert($d);
        }
    }
}
