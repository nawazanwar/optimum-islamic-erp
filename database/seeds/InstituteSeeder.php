<?php

use Illuminate\Database\Seeder;
use App\Models\Institute;
use Illuminate\Support\Facades\DB;
use App\Models\Branch;
use Illuminate\Support\Str;

class InstituteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $institute = new Institute();
        $institute->name_en = 'Madrissa Rizwan';
        $institute->name_ur = 'مدریس رضوان';
        $institute->slug = 'madrissa-rizwan';
        $institute->location_en = 'Noorpur Rd, Noor Pur Gulistan Colony 2, Faisalabad, Punjab';
        $institute->location_ur = 'نور پور آر ڈی ، نور پور گلستان کالونی 2 ، فیصل آباد ، پنجاب';
        if ($institute->save()) {
            /* Add Credentials*/
            $institute->credentials()->save(new \App\Models\Credential([
                'domain_name' => 'http://madrissa.local/',
                'database_name' => 'madrissa',
                'database_user' => 'root',
                'database_pwd' => '',
            ]));
            /* Add Services */
            DB::table('institute_service')->insert([
                [
                    'service_id' => 1,
                    'institute_id' => $institute->id
                ],
                [
                    'service_id' => 2,
                    'institute_id' => $institute->id
                ],
                [
                    'service_id' => 3,
                    'institute_id' => $institute->id
                ],
                [
                    'service_id' => 4,
                    'institute_id' => $institute->id
                ],
                [
                    'service_id' => 5,
                    'institute_id' => $institute->id
                ]

            ]);

            $institute->branches()->saveMany([
                new Branch([
                    'name_en' => 'First Branch',
                    'name_ur' => 'پہلی شاخ',
                    'slug' => Str::slug("First Branch", '-'),
                    'location_en' => 'Jahangir bazaar Faisalabad',
                    'location_ur' => 'جہانگیر بازار فیصل آباد'
                ]),
                new Branch([
                    'name_en' => 'Second Branch',
                    'name_ur' => 'دوسری شاخ',
                    'slug' => Str::slug("Second Branch", '-'),
                    'location_en' => 'Batala Colony Faisalabad',
                    'location_ur' => 'بٹالہ کالونی فیصل آباد'
                ])
            ]);
        }
    }
}
