<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCredentialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credentials', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('institute_id')->unsigned()->nullable();
            $table->string('hosting_name')->nullable();
            $table->string('hosting_user')->nullable();
            $table->string('hosting_pwd')->nullable();
            $table->string('domain_name')->nullable();
            $table->string('database_name')->nullable();
            $table->string('database_user')->nullable();
            $table->string('database_pwd')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('institute_id')
                ->references('id')
                ->on('institutes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credentials');
    }
}
