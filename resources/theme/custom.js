var Ajax = new function () {
    this.call = function (url, data, method = 'GET', callback) {
        Ajax.setAjaxHeader();
        $.ajax({
            type: method,
            global: false,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            url: url,
            data: data,
            success: function (response) {
                callback(response);
            },
            error: function (response) {
                $.each(response.responseJSON.errors, function (key, value) {
                    toastr.error(value[0]);
                });
            }
        });
    };
    this.setAjaxHeader = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            }
        });
    };
};


var CrossAjax = new function () {
    this.call = function (url, data, method = 'GET', callback) {
        CrossAjax.setAjaxHeader();
        $.ajax({
            type: method,
            async: true,
            url: url,
            data: data,
            success: function (response) {
                callback(response);
            },
            error: function (response) {
                console.log(response);
            }
        });
    };
    this.setAjaxHeader = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            }
        });
    };
};


var Custom = new function () {
    this.preview = function (cElement, parent_holder) {
        var reader = new FileReader();
        reader.onload = function () {
            $(cElement).closest('.form-group').find("#" + parent_holder).find('img').attr('src', reader.result);
        };
        reader.readAsDataURL(cElement.files[0]);
    };

    this.addClone = function (cElement) {

        var $firstRow = $(cElement).closest('tbody').find('tr:nth-child(2)'); //grab row before the last row
        var $newRow = $firstRow.clone(); //clone it
        $newRow.css({'background-color': '#f4f6f9a6'});
        $newRow.find('td').find('input').val('');
        $firstRow.before($newRow); //add in the new row at the end
    };

    this.removeClone = function (cElement) {
        var $firstRow = $(cElement).closest('tbody').find('tr:nth-child(2)'); //grab row before the last row
        $firstRow.remove();
    };

    this.addCloneTbody = function (cElement) {
        var $lastTbody = $(cElement).closest('thead').next('tbody:first'); //grab row before the last row
        var $newTBody = $lastTbody.clone(); //clone it
        $newTBody.find('td').find('input').val('');
        $newTBody.find('td').find('.id_field').remove();
        $lastTbody.before($newTBody); //add in the new row at the end

    };

    this.removeCloneTbody = function (cElement) {
        var $lastTBody = $(cElement).closest('thead').next('tbody:first');
        $lastTBody.remove();
    };

    this.manage_check_box_value = function (cElement) {
        if ($(cElement).prop('checked') == false) {
            $(cElement).removeAttr('checked').val(0);
        } else {
            $(cElement).attr('checked', true).val(1);
        }
    };

    this.changeStatus = function (input) {
        var route = $(input).attr('href');
        Ajax.call(route, null, 'GET', function (response) {
            if (response.type == 'active') {
                $(input).removeClass('bg-success').addClass('bg-danger').empty().append('<i class="fa fa-times text-white"></i>');
                route = route.replace('active', 'in_active');
                toastr.warning('Successfully Inactive');
            } else {
                $(input).addClass('bg-success').removeClass('bg-danger').empty().append('<i class="fa fa-check text-white"></i>');
                route = route.replace('in_active', 'active');
                toastr.success('Successfully Activate');
            }
            $(input).removeAttr('href').attr('href', route);
        });
    };

    this.changeBranchStatus = function (input) {
        var route = $(input).attr('href');
        Ajax.call(route, null, 'GET', function (response) {
            if (response.type == 'active') {
                $(input).removeClass('bg-success').addClass('bg-danger').empty().append('<i class="fa fa-times text-white"></i>');
                route = route.replace('active', 'in_active');
                toastr.warning('Successfully Inactive');
            } else {
                $(input).addClass('bg-success').removeClass('bg-danger').empty().append('<i class="fa fa-check text-white"></i>');
                route = route.replace('in_active', 'active');
                toastr.success('Successfully Activate');
            }
            $(input).removeAttr('href').attr('href', route);
        });
    }

};

