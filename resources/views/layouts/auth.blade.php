<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('pageTitle')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('dashboard/css/vendor.min.css') }}">
@yield('styleInnerFiles')
<body class="login-page" cz-shortcut-listen="true" style="min-height: 512.391px;">
<main>
    @yield('content')
    <script src="{{ asset('dashboard/js/vendor.min.js') }}"></script>
    @yield('scriptInnerFiles')
    @yield('pageScript')
</main>
</body>
</html>
