<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('pageTitle')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('website/css/vendor.min.css') }}">
<body>
@include('partials.website.header')
<main>
    @yield('content')
    <script src="{{ asset('website/js/vendor.min.js') }}"></script>
</main>
@include('partials.website.footer')
</body>
</html>
