@php
    $dir = (app()->getLocale()!='en')?'rtl':'ltr';
@endphp
    <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('dashboard/css/vendor.min.css') }}" rel="stylesheet">
    {{-- Innser Style Files--}}
    @yield('styleInnerFiles')
    <title>@yield('pageTitle')</title>
    @stack('style')
</head>
<body class="sidebar-mini" style="height: auto;">
{{-- start Main Wrapper--}}
<div class="wrapper">
    {{-- Include Header--}}
    @include('partials.dashboard.header')
    {{-- Left Bar for Navigation--}}
    @include('partials.dashboard.left-bar')
    {{-- Main Section--}}
    <div class="content-wrapper" style="min-height: 1200.88px;">
        @yield('breadcrumbs')
        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>
    {{--Include Footer--}}
    @include('partials.dashboard.footer')
    {{-- Control sidebar--}}
    @include('partials.dashboard.control-sidebar')
</div>
<script src="{{ asset('dashboard/js/vendor.min.js') }}"></script>
@yield('scriptInnerFiles')
@yield('pageScript')
</body>
</html>
