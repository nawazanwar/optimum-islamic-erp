@extends('layouts.auth')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div class="register-box">
        <div class="card">
            <div class="card-body login-card-body">
                <h5 class="login-box-msg">{{ __('auth.register.info') }}</h5>

                {{-- Start Form Management--}}

                {!! Form::open(['route' => ['register'], 'method' => 'POST']) !!}
                {!! csrf_field() !!}

                <div class="row">
                    <div class="input-group mb-3 col-md-6">
                        <input type="text" name="fname" dir="{{ app()->getLocale()=='ur'?'rtl':'ltr' }}"
                               class="form-control" value="{{ old('first_name') }}"
                               placeholder="{{ __('placeholder.first_name') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback show"
                                  role="alert"><strong>{{ $errors->first('first_name') }}</strong></span>
                        @endif
                    </div>
                    <div class="input-group mb-3 col-md-6">
                        <input type="text" name="last_name" dir="{{ app()->getLocale()=='ur'?'rtl':'ltr' }}"
                               class="form-control" value="{{ old('last_name') }}"
                               placeholder="{{ __('placeholder.last_name') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                        @if ($errors->has('last_name'))
                            <span class="invalid-feedback show"
                                  role="alert"><strong>{{ $errors->first('last_name') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="input-group mb-3 col-12">
                                <input type="email" name="email" dir="{{ app()->getLocale()=='ur'?'rtl':'ltr' }}"
                                       class="form-control" value="{{ old('email') }}"
                                       placeholder="{{ __('placeholder.email') }}">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-envelope"></span>
                                    </div>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback show"
                                          role="alert"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                            </div>
                            <div class="input-group mb-3 col-12">
                                <input type="password" name="password"
                                       dir="{{ app()->getLocale()=='ur'?'rtl':'ltr' }}"
                                       class="form-control" value="{{ old('password') }}"
                                       placeholder="{{ __('placeholder.password') }}">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-lock"></span>
                                    </div>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback show"
                                          role="alert"><strong>{{ $errors->first('password') }}</strong></span>
                                @endif
                            </div>
                            <div class="input-group mb-3 col-12">
                                <input type="password" name="confirm_password"
                                       dir="{{ app()->getLocale()=='ur'?'rtl':'ltr' }}"
                                       class="form-control" value="{{ old('confirm_password') }}"
                                       placeholder="{{ __('placeholder.confirm_password') }}">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-lock"></span>
                                    </div>
                                </div>
                                @if ($errors->has('confirm_password'))
                                    <span class="invalid-feedback show"
                                          role="alert"><strong>{{ $errors->first('confirm_password') }}</strong></span>
                                @endif
                            </div>
                            <div class="mb-3 col-12">
                                {!! Form::select('roles[]', $roles, null,
                                    [
                                    'class' => 'form-control selectpicker show-menu-arrow',
                                    'data-live-search' => 'true',
                                    'data-selected-text-format' => 'count > 3',
                                    'data-size' => '10',
                                    'data-actions-box' => 'true',
                                    'multiple' => 'multiple'
                                    ]) !!}
                                @if ($errors->has('roles'))
                                    <span class="invalid-feedback show"
                                          role="alert"><strong>{{ $errors->first('roles') }}</strong></span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-12 mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="avatar"
                                           onchange="Custom.preview(this,'image_preview')">
                                    <label class="custom-file-label" for="avatar">{{__('btn.choose_file')}}</label>
                                </div>
                            </div>
                            <div class="col-md-7" id="image_preview">
                                <img class="img-thumbnail" src="{{ asset('defaults/no_preview.jpg') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-3">
                    <div class="col-12 input-group">
                        {!! Form::submit(__('btn.login'), array('class' => 'btn btn-primary btn-block')) !!}
                    </div>
                </div>

                {!! Form::close() !!}

                {{-- End Form Management --}}

                <div class="social-auth-links text-center mb-3">
                    <p>- {{__('auth.or')}} -</p>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-block btn-primary">
                                @if(app()->getLocale()=='ur')
                                    {{ __('btn.register_with_facebook') }} <i class="fab fa-facebook mr-2"></i>
                                @else
                                    <i class="fab fa-facebook mr-2"></i> {{ __('auth.register_with_facebook') }}
                                @endif
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="#" class="btn btn-block btn-info">
                                @if(app()->getLocale()=='ur')
                                    {{ __('btn.register_with_twitter') }} <i class="fab fa-twitter mr-2"></i>
                                @else
                                    <i class="fab fa-twitter mr-2"></i> {{ __('auth.register_with_twitter') }}
                                @endif
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /.social-auth-links -->
                <p class="mb-0 {{ app()->getLocale()=='en'?'text-left':'text-right' }}">
                    <a href="{{ route('login') }}" class="text-center">
                        {{__('auth.already_register')}}
                    </a>
                </p>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
@endsection
@section('scriptInnerFiles')
    <script type="text/javascript"
            src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
@stop
