@extends('layouts.auth')
@section('pageTitle', $pageTitle)
@section('content')
    <div class="login-box">
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">{{ __('auth.login.info') }}</p>
                <form action="{{ route('login')  }}" method="post" id="loginForm" onsubmit="return false;">
                    @csrf
                    <div class="form-group">
                        <input type="email" id="email" name="email" dir="{{ app()->getLocale()=='ur'?'rtl':'ltr' }}"
                               class="form-control" value="{{ old('email') }}"
                               placeholder="{{ __('placeholder.email') }}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback show"
                                  role="alert"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="password" id="password" name="password" class="form-control"
                               dir="{{ app()->getLocale()=='ur'?'rtl':'ltr' }}" value="{{ old('password') }}"
                               placeholder="{{ __('placeholder.password') }}">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback show"
                                  role="alert"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="icheck-primary">
                                <input type="checkbox" id="remember">
                                <label for="remember">
                                    {{ __('auth.login.remember') }}
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-primary">{{__('btn.login')}}</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

                {{-- Start social Link --}}

                <div class="social-auth-links text-center">
                    <p>- {{__('auth.or')}} -</p>
                    <div class="row">
                        <div class="col-md-12 my-1">
                            <a href="#" class="btn btn-block btn-primary">
                                @if(app()->getLocale()=='ur')
                                    {{ __('btn.login_with_facebook') }} <i class="fab fa-facebook mr-2"></i>
                                @else
                                    <i class="fab fa-facebook mr-2"></i> {{ __('btn.login_with_facebook') }}
                                @endif
                            </a>
                        </div>
                        <div class="col-md-12 my-1">
                            <a href="#" class="btn btn-block btn-info">
                                @if(app()->getLocale()=='ur')
                                    {{ __('btn.login_with_twitter') }} <i class="fab fa-twitter mr-2"></i>
                                @else
                                    <i class="fab fa-twitter mr-2"></i> {{ __('btn.login_with_twitter') }}
                                @endif
                            </a>
                        </div>
                    </div>
                </div>

                {{-- End Social Link--}}

                <p class="mb-1 {{ app()->getLocale()=='en'?'text-left':'text-right' }}">
                    <a href="forgot-password.html">{{__('auth.login.forgot_password')}}</a>
                </p>
                <hr class="mb-2">
                <p class="mb-0 {{ app()->getLocale()=='en'?'text-left':'text-right' }}">
                    <a href="{{ route('register') }}" class="text-center">
                        {{__('auth.already_not_register')}}
                    </a>
                </p>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
@endsection
@section('scriptInnerFiles')
    <script type="text/javascript"
            src="{{ asset('plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>
@stop
@section('pageScript')
    <script>
        (function ($) {

            $("#loginForm").validate({
                rules: {
                    email: "required",
                    password: "required",
                },
                messages: {
                    email: {
                        required: "{{ __('auth.email_required') }}",
                    },
                    email: {
                        required: "{{ __('auth.password_required') }}",
                    },

                }, submitHandler: function (form) {

                    form.submit();
                }
            });
        })(jQuery)
    </script>
@endsection

