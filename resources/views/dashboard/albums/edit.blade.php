@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="card card-solid">
        @can('read',\App\Models\User::class)
            <div class="card-header">
                <div class="card-title">
                    {!! link_to_route('venues.index',__('system.all_venues'),null,['class'=>'btn btn-info btn-sm']) !!}
                </div>
            </div>
        @endcan
        <div class="card-body pb-0">
            <!-- form start -->
            {!! Form::model($model, ['route' => ['venues.update', $model], 'method' => 'PUT','files' => true] ) !!}
            {!! csrf_field() !!}
            @include('partials.dashboard.message')

            <div class="form-group">
                {!! Form::label('venue_owner', 'Assign Owner') !!}
                {!! Form::select('user_id', $users, null, array('id' => 'venue_owner',
                'class' => 'form-control selectpicker show-menu-arrow', 'data-live-search' => 'true', 'data-selected-text-format' => 'count > 3',
                'data-size' => '10', 'data-actions-box' => 'true')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('inputName', __('system.name')) !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'autofocus','placeholder'=>__('system.placeholder_name'), 'id' => 'inputName' ]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('inputLocation', __('system.location')) !!}
                {!! Form::text('location', null, ['class' => 'form-control', 'id' => 'inputLocation','placeholder'=>__('system.placeholder_location') ]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('inputCapacity', __('system.capacity')) !!}
                {!! Form::text('capacity', null, ['class' => 'form-control', 'id' => 'inputCapacity','placeholder'=>__('system.placeholder_capacity') ]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('avatar', 'Choose Avatar') !!}
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="avatar" id="avatar" onchange="loadFile(event)">
                        <label class="custom-file-label" for="avatar">Choose Image</label>
                    </div>
                </div>
                <div class="p-3 text-center">
                    <img style="max-width: 100px;max-height: 100px;" src="{{ $model->getAvatar() }}">
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('description', 'Description') !!}
                {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 5, 'id' => 'description' ]) !!}
            </div>
            <div class="form-group icheck-success d-inline">
                {!! Form::checkbox('active', 1, true,['id'=>'inputActive']) !!}
                {!! Form::label('inputActive', __('system.is_active')) !!}
            </div>

            <div class="form-group text-right">
                {!! Form::submit(__('system.update'), array('class' => 'btn btn-primary pull-left btn-sm')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script type="text/javascript"
            src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=o23blg9ah1zca8fzz4hpnqndoxanbov1pv288bvw0ptznp19"></script>
@stop
@section('pageScript')
    <script>
        var loadFile = function (event) {
            var reader = new FileReader();
            reader.onload = function () {
                $(event.target).closest('.form-group').find('img').attr('src', reader.result);
            };
            reader.readAsDataURL(event.target.files[0]);
        };
        $(function () {
            // Replace the textarea with tinyMCE
            tinymce.init({
                selector: '#description',
                height: 200,
                theme: 'modern'
            });
        });
    </script>
@endsection
