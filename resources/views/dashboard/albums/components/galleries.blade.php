<div class="gallery_holder">
    <div class="dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
            <i class="fa fa-eye"></i>
            <span class="badge badge-danger navbar-badge">{{ $d->galleries->count()}}</span>
        </a>
        @if($d->galleries->count()>0)
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0;">
                <div class="row">
                    @foreach($d->galleries as $gallery)
                        <div class="col-md-6">
                            <img src="{{ $gallery->avatar() }}" class="img-thumbnail">
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
</div>
