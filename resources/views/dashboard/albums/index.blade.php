@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-xl-6 col-lg-6 col-md-6">
                        @can('create',\App\Models\Album::class)
                            {!! link_to_route('albums.create',__('album.create'),null,['class'=>'btn bg-gradient-primary btn-sm']) !!}
                        @endcan
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 text-right">
                        <form action="{{ route('albums.index') }}" method="get">
                            <div class="row justify-content-end">
                                <div class="col-xl-5">
                                    <select class="custom-select custom-select-sm" name="field">
                                        <option value="name" @if(isset($field) and $field == 'name') selected @endif>
                                            {{__('general.name')}}
                                        </option>
                                    </select>
                                </div>
                                <div class="col-xl-5">
                                    <input type="search" name="keyword"
                                           value="@if(isset($keyword) and $keyword != '') {{ $keyword }} @endif"
                                           class="form-control form-control-sm"
                                           placeholder="{{__('btn.search_here')}}">
                                </div>
                                <div class="col-xl-2">
                                    <input type="submit" class="btn btn-info btn-sm" value="{{ __('btn.filter') }}">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body">
                    @include('partials.dashboard.message')
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">{{__('general.avatar')}}</th>
                            <th>{{__('general.name')}}</th>
                            <th class="text-right">{{__('general.name_ur')}}</th>
                            <th class="text-center">{{__('general.is_active')}}</th>
                            <th class="text-center">{{__('general.galleries')}}</th>
                            <th class="text-center">{{ __('general.created_at') }}</th>
                            <th class="text-center">{{ __('general.last_modified') }}</th>
                            <th class="text-center">{{ __('general.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data)>0)
                            @foreach($data as $d)
                                <tr>
                                    <td class="text-center m-auto">
                                        <img class="img-circle img-thumbnail img-sm" src="{{$d->avatar()}}">
                                    </td>
                                    <td>{{$d->name}}</td>
                                    <td class="text-right">{{$d->name_ur}}</td>
                                    <td class="text-center">
                                        @if($d->active)
                                            <span class="badge badge-success">
                                                <i class="fa fa-check"></i>
                                            </span>
                                        @else
                                            <span class="badge badge-danger">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @include('dashboard.albums.components.galleries')
                                    </td>
                                    <td class="text-center">{{$d->created_at}}</td>
                                    <td class="text-center">{{$d->updated_at->diffForHumans()}}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default">Action</button>
                                            <button type="button" class="btn btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                @can('edit',\App\Models\Album::class)
                                                    <li class="dropdown-item">{!! link_to_route('albums.edit', __('btn.edit'), [$d->id]) !!}</li>
                                                @endif
                                                @can('delete',\App\Models\Album::class)
                                                    <form action="{{ route('albums.destroy',$d->id) }}"
                                                          method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <li class="dropdown-item">
                                                            <button type="submit" class="bg-transparent"
                                                                    onclick="return confirm('{{__('message.dou_you_really_wants_to_delete')}}')"
                                                                    style="border: none;padding-left: 0;color: #c13535;">
                                                                {{__('btn.delete')}}
                                                            </button>
                                                        </li>
                                                    </form>
                                                @endcan
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="12" class="p-3">{{ __('message.no_record_found') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix">
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
@stop
