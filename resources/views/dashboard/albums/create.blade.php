@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop
@section('content')
    <div class="card card-solid">
        {!! Form::open(['route' => ['albums.store'], 'method' => 'POST','files' => true,'id'=>'createForm']) !!}
        {!! csrf_field() !!}
        @can('read',\App\Models\Album::class)
            <div class="card-header row">
                <div class="card-title col-6">
                    {!! link_to_route('albums.index',__('album.title'),null,['class'=>'btn btn-info btn-sm']) !!}
                </div>
                <div class="card-title col-6 text-right">
                    {!! Form::submit(__('btn.save'), array('class' => 'btn btn-primary pull-left btn-sm')) !!}
                </div>
            </div>
        @endcan
        @include('partials.dashboard.message')
        <div class="card-body pb-0">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('name', __('album.name')) !!}
                        {!! Form::text('name', old('name'), ['class' => 'form-control', 'autofocus','placeholder'=>__('album.placeholder.name'), 'id' => 'name' ]) !!}
                    </div>
                    <div class="form-group text-right">
                        {!! Form::label('name_ur', __('album.name_ur')) !!}
                        {!! Form::text('name_ur', old('name_ur'), ['class' => 'form-control', 'autofocus','placeholder'=>__('album.placeholder.name_ur'), 'id' => 'name_ur','dir'=>
    'rtl']) !!}
                    </div>
                    <div class="form-group icheck-success d-inline">
                        {!! Form::checkbox('active', 1, true,['id'=>'inputActive']) !!}
                        {!! Form::label('inputActive', __('general.is_active')) !!}
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <div class="form-group">
                        {!! Form::label('avatar',__('album.avatar'),['class'=>'text-right']) !!}
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="album_avatar" id="avatar"
                                       onchange="Custom.preview(this,'preview_holder')">
                                <label class="custom-file-label" for="avatar">{{__('general.choose_image')}}</label>
                            </div>
                        </div>
                        <div class="p-3 text-center" id="preview_holder">
                            <img height="200" width="150" class="img-thumbnail"
                                 src="{{ asset('defaults/no_preview.jpg') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-header text-center">
                <strong>{{ __('album.images.list') }}</strong>
            </div>
            <div class="row">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td colspan="4" class="text-right">
                            <a class="btn btn-primary btn-xs text-white"
                               onclick="Custom.addCloneTbody(this);return false;"><i
                                    class="fa fa-plus"></i></a>
                            <a class="btn btn-danger btn-xs text-white"
                               onclick="Custom.removeCloneTbody(this);return false;"><i
                                    class="fa fa-minus"></i></a>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div class="form-group">
                                {!! Form::label('image',__('album.images.avatar')) !!}
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="galleries[]"
                                               onchange="Custom.preview(this,'image_holder')">
                                        <label class="custom-file-label">{{ __('album.images.choose_avatar') }}</label>
                                    </div>
                                </div>
                                <div class="p-3 text-center" id="image_holder">
                                    <img height="200" width="150" class="img-thumbnail"
                                         src="{{ asset('defaults/no_preview.jpg') }}">
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                {!! Form::label('image',__('album.images.avatar')) !!}
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="galleries[]"
                                               onchange="Custom.preview(this,'image_holder')">
                                        <label class="custom-file-label">{{ __('album.images.choose_avatar') }}</label>
                                    </div>
                                </div>
                                <div class="p-3 text-center" id="image_holder">
                                    <img height="200" width="150" class="img-thumbnail"
                                         src="{{ asset('defaults/no_preview.jpg') }}">
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                {!! Form::label('image',__('album.images.avatar')) !!}
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="galleries[]"
                                               onchange="Custom.preview(this,'image_holder')">
                                        <label class="custom-file-label">{{ __('album.images.choose_avatar') }}</label>
                                    </div>
                                </div>
                                <div class="p-3 text-center" id="image_holder">
                                    <img height="200" width="150" class="img-thumbnail"
                                         src="{{ asset('defaults/no_preview.jpg') }}">
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                {!! Form::label('image',__('album.images.avatar')) !!}
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="galleries[]"
                                               onchange="Custom.preview(this,'image_holder')">
                                        <label class="custom-file-label">{{ __('album.images.choose_avatar') }}</label>
                                    </div>
                                </div>
                                <div class="p-3 text-center" id="image_holder">
                                    <img height="200" width="150" class="img-thumbnail"
                                         src="{{ asset('defaults/no_preview.jpg') }}">
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('scriptInnerFiles')
    <script type="text/javascript"
            src="{{ asset('plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('plugins/multiform/js/style.min.js') }}"></script>
@stop
@section('pageScript')
    <script>
        (function ($) {
            $("#createForm").validate({
                rules: {
                    name: "required",
                    name_ur: "required"
                }, submitHandler: function (form) {
                    Ajax.setAjaxHeader();
                    var formData = new FormData(form);
                    Ajax.call("{{ route('albums.store') }}", formData, 'POST', function (response) {
                        if (response.status == 200) {
                            toastr.success(response.message);
                            window.location.href = "{{URL::to('dash/albums')}}"
                        }
                    })
                }
            });
        })(jQuery)

    </script>
@endsection
