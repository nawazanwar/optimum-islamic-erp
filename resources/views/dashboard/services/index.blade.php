@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop
@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-body">
                    @include('partials.dashboard.message')
                    {!! Form::open(['route' => 'services.store', 'files' => true] ) !!}
                    {!! csrf_field() !!}
                    <table class="table table-bordered table-condenced">
                        <tbody>
                        @if(count($data)>0)
                            <tr>
                                <td>
                                    {!! Form::submit(__('btn.save'), array('class' => 'btn btn-primary')) !!}
                                </td>
                                <td colspan="3" class="text-right">
                                    <a class="btn btn-primary btn-xs text-white"
                                       onclick="Custom.addClone(this);return false;"><i
                                            class="fa fa-plus"></i></a>
                                    <a class="btn btn-danger btn-xs text-white"
                                       onclick="Custom.removeClone(this);return false;"><i
                                            class="fa fa-minus"></i></a>
                                </td>
                            </tr>
                            @foreach($data as $d)
                                <tr>
                                    <td>
                                        {!! Form::label('name_en', __('service.name')) !!}
                                        <input type="text" name="name_en[]" class="form-control" value="{{ $d->name_en }}"
                                               placeholder="{{ __('service.placeholders.name') }}" id="name_en_{{$d->id}}">
                                    </td>
                                    <td class="text-right">
                                        {!! Form::label('name_ur', __('service.name_ur')) !!}
                                        <input type="text" name="name_ur[]" class="form-control" dir="rtl"
                                               value="{{ $d->name_ur }}"
                                               placeholder="{{ __('service.placeholders.name_ur') }}"
                                               id="name_ur_{{$d->id}}">
                                    </td>
                                    <td>
                                        {!! Form::label('slug', __('service.slug')) !!}
                                        <input type="text" name="slug[]" class="form-control" value="{{ $d->slug }}"
                                               placeholder="{{ __('service.placeholders.slug') }}" id="slug_{{$d->id}}">
                                    </td>
                                    <td>
                                        <div class="form-group clearfix">
                                            <div class="icheck-success d-inline">
                                                <input type="checkbox" value="{{ $d->active?true:false }}"
                                                       onchange="Custom.manage_check_box_value(this);"
                                                       @if($d->active) checked @endif name="active[]"
                                                       id="active-{{$d->id}}">
                                                <label for="active-{{$d->id}}"></label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
