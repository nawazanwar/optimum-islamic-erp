@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)
@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs')
@stop
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <a href="compose.html"
                   class="btn btn-primary btn-block mb-3">{{__('notification.generate_for_all')}}</a>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{__('notification.folders')}}</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                    class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item active">
                                <a href="#" class="nav-link">
                                    <i class="fas fa-inbox"></i> {{__('notification.inbox')}}
                                    <span class="badge bg-primary float-right">12</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="far fa-envelope"></i> {{__('notification.sent')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="far fa-file-alt"></i> {{__('notification.draft')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="fas fa-filter"></i> {{__('notification.junk')}}
                                    <span class="badge bg-warning float-right">65</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="far fa-trash-alt"></i> {{__('notification.trash')}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">{{__('notification.inbox')}}</h3>
                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" placeholder="{{__('notification.filter')}}">
                                <div class="input-group-append">
                                    <div class="btn btn-primary">
                                        <i class="fas fa-search"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="table-responsive mailbox-messages">
                            <table class="table table-hover table-striped">
                                <tbody>
                                @if($data->count()>0)
                                    @foreach($data as $unreadNotification)
                                        <tr>
                                            <td>
                                                <div class="icheck-primary">
                                                    <input type="checkbox" value="{{ $unreadNotification->id }}"
                                                           id="check-{{$unreadNotification->id}}">
                                                    <label for="check-{{$unreadNotification->id}}"></label>
                                                </div>
                                            </td>
                                            <td class="mailbox-name">
                                                <a href="{{ route('institutes.show',[$unreadNotification['data']['sender']]) }}">
                                                    {{ \App\Models\Institute::find($unreadNotification['data']['sender'])->name_ur }}
                                                </a>
                                            </td>
                                            <td class="mailbox-subject">
                                                {{ $unreadNotification['data']['message'] }}
                                            </td>
                                            <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
                                            <td class="mailbox-date">{{ $unreadNotification->created_at->diffForHumans() }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer p-0">
                        <div class="mailbox-controls">
                            {{ $data->links()  }}
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
