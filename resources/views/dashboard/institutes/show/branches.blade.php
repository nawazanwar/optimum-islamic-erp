@if($data->branches->count()>0)
    @foreach($data->branches as $branch)
        <div class="timeline timeline-inverse">
            <div>
                <i class="fas fa-building bg-primary"></i>
                <div class="timeline-item">
                    <span class="time text-right"><i class="far fa-clock"></i>{{ $branch->created_at->diffForHumans() }}</span>
                    <h3 class="timeline-header">{{ $branch->name_ur }}</h3>
                    <div class="timeline-body bg-white">
                        <p class="text-left font-weight-bold">
                            <i class="fas fa-book mr-1"></i> {{__('institute.branch.location_en')}}
                        </p>
                        <p class="text-muted text-left">{{ $branch->location_en }}</p>
                        <hr>
                        <p class="text-right font-weight-bold">
                            <i class="fas fa-book mr-1"></i> {{__('institute.branch.location_ur')}}
                        </p>
                        <p class="text-muted text-right">{{ $branch->location_ur }}</p>
                    </div>
                    <div class="timeline-footer text-center">
                        @include('dashboard.institutes.components.active-de-active-branch',['data'=>$branch])
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
