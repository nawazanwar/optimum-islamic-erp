@if($data->services->count()>0)
    @foreach($data->services as $service)
        <div class="timeline timeline-inverse">
            <div>
                <i class="fas fa-envelope bg-primary"></i>
                <div class="timeline-item">
                    <span class="time"><i class="far fa-clock"></i>{{ $service->created_at->diffForHumans() }}</span>
                    <h3 class="timeline-header">{{ $service->slug }}</h3>
                    <div class="timeline-body bg-white">
                        <p class="text-left font-weight-bold">
                            <i class="fas fa-book mr-1"></i> {{__('service.name_en')}}
                        </p>
                        <p class="text-muted text-left">{{ $service->name_en }}</p>
                        <hr>
                        <p class="text-right font-weight-bold">
                            <i class="fas fa-book mr-1"></i> {{__('service.name_ur')}}
                        </p>
                        <p class="text-muted text-right">{{ $service->name_ur }}</p>
                    </div>
                    <div class="timeline-footer text-center">
                        @if($service->active)
                            <a class="btn btn-success btn-sm">{{__('btn.active')}}</a>
                        @else
                            <a class="btn btn-danger btn-sm">{{__('btn.in_active')}}</a>
                        @endif

                        @if($service->pivot->is_sync)
                            <a class="btn btn-success btn-sm">Sync</a>
                        @else
                            <a class="btn btn-danger btn-sm">Not Sync</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
