@if($data->credentials->count()>0)
    <div class="timeline timeline-inverse">
        <div>
            <i class="fas fa-envelope bg-primary"></i>
            <div class="timeline-item">
                <div class="timeline-body bg-white">
                    {{-- start credentials--}}
                    <div class="nav nav-tabs"
                         role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" data-toggle="pill"
                           href="#vert-tabs-hosting" role="tab"
                           aria-controls="vert-tabs-hosting"
                           aria-selected="true">{{__('institute.credential.hosting')}}</a>
                        <a class="nav-link" data-toggle="pill"
                           href="#vert-tabs-domain" role="tab"
                           aria-controls="vert-tabs-domain"
                           aria-selected="true">{{__('institute.credential.domain')}}</a>
                        <a class="nav-link" data-toggle="pill"
                           href="#vert-tabs-database" role="tab"
                           aria-controls="vert-tabs-database"
                           aria-selected="true">{{__('institute.credential.database')}}</a>
                    </div>
                    <div class="tab-content my-3">
                        <div class="tab-pane text-left fade show active"
                             id="vert-tabs-hosting" role="tabpanel">
                            <div class="form-group">
                                {!! Form::label('hosting_name', __('institute.credential.hosting_name')) !!}
                                {!! Form::text('hosting_name',$data->credentials[0]->hosting_name, ['class' => 'form-control', 'id' => 'hosting_name']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('hosting_user', __('institute.credential.hosting_user')) !!}
                                {!! Form::text('hosting_user',$data->credentials[0]->hosting_user, ['class' => 'form-control', 'id' => 'hosting_user']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('hosting_pwd', __('institute.credential.hosting_pwd')) !!}
                                {!! Form::text('hosting_pwd', $data->credentials[0]->hosting_pwd, ['class' => 'form-control', 'id' => 'hosting_pwd']) !!}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="vert-tabs-domain"
                             role="tabpanel">
                            <div class="form-group">
                                {!! Form::label('domain_name', __('institute.credential.domain_name')) !!}
                                {!! Form::text('domain_name', $data->credentials[0]->domain_name, ['class' => 'form-control', 'id' => 'domain_name']) !!}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="vert-tabs-database"
                             role="tabpanel">
                            <div class="form-group">
                                {!! Form::label('database_name', __('institute.credential.database_name')) !!}
                                {!! Form::text('database_name', $data->credentials[0]->database_name, ['class' => 'form-control', 'id' => 'database_name']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('database_user', __('institute.credential.database_user')) !!}
                                {!! Form::text('database_user', $data->credentials[0]->database_user, ['class' => 'form-control', 'id' => 'database_user']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('database_pwd', __('institute.credential.database_pwd')) !!}
                                {!! Form::text('database_pwd', $data->credentials[0]->hosting_pwd, ['class' => 'form-control', 'id' => 'database_pwd']) !!}
                            </div>
                        </div>
                    </div>
                    {{--end Credentials--}}
                </div>
            </div>
        </div>
    </div>
@endif
