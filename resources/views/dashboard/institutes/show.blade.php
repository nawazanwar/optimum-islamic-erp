@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle"
                                     src="{{ $data->getAvatar() }}" alt="User profile picture">
                            </div>
                            <h3 class="profile-username text-center">{{ $data->name_ur }}</h3>
                            <h3 class="profile-username text-center">{{ $data->name_en }}</h3>
                            <p class="text-muted text-center">{{ $data->slug }}</p>
                            <p class="text-center">
                                @include('dashboard.institutes.components.active-de-active',['data'=>$data])
                            </p>
                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>{{__('institute.owners')}}</b> <a
                                        class="float-right">{{ $data->owners->count() }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>{{__('institute.branches')}}</b> <a
                                        class="float-right">{{ $data->branches->count() }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>{{__('institute.our_services')}}</b> <a
                                        class="float-right">{{ $data->services->count() }}</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- About Me Box -->
                    <div class="card card-primary">
                        <div class="card-header text-center">
                            <h3 class="card-title">{{__('institute.profile')}}</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <p class="text-center font-weight-bold">
                                <i class="fas fa-book mr-1"></i>{{__('institute.location_en')}}
                            </p>
                            <p class="text-muted">
                                {{ $data->location_en }}
                            </p>
                            <hr>
                            <p class="text-center font-weight-bold">
                                <i class="fas fa-book mr-1"></i>{{__('institute.location_ur')}}
                            </p>
                            <p class="text-muted">
                                {{ $data->location_ur }}
                            </p>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#branches" data-toggle="tab">
                                        {{__('institute.branches')}}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#services" data-toggle="tab">
                                        {{__('institute.our_services')}}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#domain_hosting" data-toggle="tab">
                                        {{__('institute.domain_hosting_info')}}
                                    </a>
                                </li>
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                {{-- Branches--}}
                                <div class="tab-pane active" id="branches">
                                    @include('dashboard.institutes.show.branches')
                                </div>
                                {{-- Serrvices--}}
                                <div class="tab-pane" id="services">
                                    @include('dashboard.institutes.show.services')
                                </div>
                                {{-- Credentials--}}
                                <div class="tab-pane" id="domain_hosting">
                                    @include('dashboard.institutes.show.credentials')
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@stop
