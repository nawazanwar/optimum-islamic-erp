@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
@endsection
@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <form method="post"
                      enctype="multipart/form-data" id="createForm" onsubmit="return false;">
                    {{ csrf_field() }}
                    <div class="card card-primary card-outline">
                        <div class="card-body p-0">
                            <div class="mailbox-controls text-right">
                                <button type="submit" class="btn btn-info btn-sm"><i class="fas fa-sync-alt"></i>
                                </button>
                            </div>
                            <div class="table-responsive mailbox-messages">
                                <table class="table table-hover table-striped">
                                    <tbody>
                                    @if($data->services()->count()>0)
                                        @foreach($data->services as $service)
                                            <tr>
                                                <td>
                                                    <div class="icheck-primary">
                                                        <input type="checkbox" value="{{ $service->id }}"
                                                               id="check-{{$service->id}}" name="services[]"
                                                               @if($service->pivot->is_sync==true) checked @endif>
                                                        <label for="check-{{$service->id}}"></label>
                                                    </div>
                                                </td>
                                                <td class="mailbox-name">
                                                    <input type="text" readonly value="{{ $service->name_en }}"
                                                           class="form-control">
                                                </td>
                                                <td class="mailbox-name">
                                                    <input type="text" readonly value="{{ $service->name_ur }}"
                                                           class="form-control">
                                                </td>
                                                <td class="mailbox-name">
                                                    <input type="text" readonly value="{{ $service->slug }}"
                                                           class="form-control">
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('scriptInnerFiles')
    <script type="text/javascript"
            src="{{ asset('plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
@stop


@section('pageScript')
    <script>
        (function ($) {

            $("#createForm").validate({
                submitHandler: function (form) {
                    Ajax.setAjaxHeader();
                    var formData = new FormData(form);
                    Ajax.call("{{ route('institute-manage-dashboard.store',['pId'=>request()->get('pId')]) }}", formData, 'POST', function (response) {
                        if (typeof response.domain[0] !== 'undefined' && response.domain[0] !== null) {
                            var child_domain = response.domain[0].domain_name + "api/institute/manage/services/" + "{{$data->id}}";
                            CrossAjax.call(child_domain, {
                                'services': response.services
                            }, 'POST', function (response) {
                                if (response.status == 200) {
                                   toastr.success("{{__('institute.message.module_added_success_fully')}}");
                                }
                            })
                        }
                    })
                }
            });

        })(jQuery)

    </script>
@endsection
