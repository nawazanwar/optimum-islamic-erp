@if($data->active)
    <a class="bg-success btn btn-xs"
       onclick="Custom.changeBranchStatus(this);return false;"
       href="{{ route('branches.status',['active',$data->id]) }}">
        <i class="fa fa-check text-white"></i>
    </a>
@else
    <a class="bg-danger btn btn-xs"
       onclick="Custom.changeBranchStatus(this);return false;"
       href="{{ route('branches.status',['in_active',$data->id]) }}">
        <i class="fa fa-times text-white"></i>
    </a>
@endif
