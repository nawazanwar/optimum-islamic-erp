<div class="row">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('institute[name_en]', __('institute.name_en')) !!}
                    {!! Form::text('institute[name_en]', old('institute[name_en]'), ['class' => 'form-control', 'autofocus', 'id' => 'institute[name_en]' ]) !!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group text-right">
                    {!! Form::label('institute[name_ur]', __('institute.name_ur')) !!}
                    {!! Form::text('institute[name_ur]', old('institute[name_ur]'), ['class' => 'form-control', 'autofocus', 'id' => 'institute[name_ur]','dir'=>'rtl']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('institute[location_en]', __('institute.location_en')) !!}
                    {!! Form::text('institute[location_en]', old('institute[location_en]'), ['class' => 'form-control', 'id' => 'institute[location_en]' ]) !!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group text-right">
                    {!! Form::label('institute[location_ur]', __('institute.location_ur')) !!}
                    {!! Form::text('institute[location_ur]', old('institute[location_ur]'), ['class' => 'form-control', 'id' => 'institute[location_ur]','dir'=>'rtl']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('institute[avatar]',__('institute.choose_image')) !!}
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="avatar" id="institute[avatar]"
                           onchange="Custom.preview(this,'image_holder')">
                    <label class="custom-file-label" for="avatar">{{ __('institute.please_choose') }}</label>
                </div>
            </div>
            <div class="p-3 text-center" id="image_holder">
                <img height="200" width="150" class="img-thumbnail"
                     src="{{ asset('defaults/no_preview.jpg') }}">
            </div>
        </div>
    </div>
</div>
