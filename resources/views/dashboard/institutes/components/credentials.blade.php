<div class="row">
    <div class="col-5 col-sm-3">
        <div class="nav flex-column nav-tabs h-100" role="tablist" aria-orientation="vertical">
            <a class="nav-link active" data-toggle="pill" href="#vert-tabs-hosting" role="tab"
               aria-controls="vert-tabs-hosting" aria-selected="true">{{__('institute.credential.hosting')}}</a>
            <a class="nav-link" data-toggle="pill" href="#vert-tabs-domain" role="tab"
               aria-controls="vert-tabs-domain" aria-selected="true">{{__('institute.credential.domain')}}</a>
            <a class="nav-link" data-toggle="pill" href="#vert-tabs-database" role="tab"
               aria-controls="vert-tabs-database" aria-selected="true">{{__('institute.credential.database')}}</a>
        </div>
    </div>
    <div class="col-7 col-sm-9">
        <div class="tab-content">
            <div class="tab-pane text-left fade show active" id="vert-tabs-hosting" role="tabpanel">
                <div class="form-group">
                    {!! Form::label('hosting_name', __('institute.credential.hosting_name')) !!}
                    {!! Form::text('hosting_name', null, ['class' => 'form-control', 'id' => 'hosting_name']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('hosting_user', __('institute.credential.hosting_user')) !!}
                    {!! Form::text('hosting_user',null, ['class' => 'form-control', 'id' => 'hosting_user']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('hosting_pwd', __('institute.credential.hosting_pwd')) !!}
                    {!! Form::text('hosting_pwd', null, ['class' => 'form-control', 'id' => 'hosting_pwd']) !!}
                </div>
            </div>
            <div class="tab-pane fade" id="vert-tabs-domain" role="tabpanel">
                <div class="form-group">
                    {!! Form::label('domain_name', __('institute.credential.domain_name')) !!}
                    {!! Form::text('domain_name', null, ['class' => 'form-control', 'id' => 'domain_name']) !!}
                </div>
            </div>
            <div class="tab-pane fade" id="vert-tabs-database" role="tabpanel">
                <div class="form-group">
                    {!! Form::label('database_name', __('institute.credential.database_name')) !!}
                    {!! Form::text('database_name', null, ['class' => 'form-control', 'id' => 'database_name']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('database_user', __('institute.credential.database_user')) !!}
                    {!! Form::text('database_user', null, ['class' => 'form-control', 'id' => 'database_user']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('database_pwd', __('institute.credential.database_pwd')) !!}
                    {!! Form::text('database_pwd', null, ['class' => 'form-control', 'id' => 'database_pwd']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
