<style>
    tbody:nth-child(odd) {
        background-color: #e9ebed29 !important;
    }
</style>
<div class="row">
    <div class="col-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <td colspan="10" class="text-right">
                    <div class="form-group mb-0 text-white">
                        <a class="btn btn-xs btn-success" onclick="Custom.addCloneTbody(this);"><i
                                class="fa fa-plus"></i></a>
                        <a class="btn btn-xs btn-danger" onclick="Custom.removeCloneTbody(this);"><i
                                class="fa fa-times"></i></a>
                    </div>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="5" class="text-center">
                    <div class="form-group">
                        {!! Form::label('owners[avatar][]',__('institute.owner.choose_image')) !!}
                        <div class="custom-file mb-2">
                            <input type="file" class="custom-file-input"
                                   name="owners[avatar][]"
                                   onchange="Custom.preview(this,'owner_prev_holder_1')"
                                   id="owners[avatar][]">
                            <div class="custom-file-label" for="owners[avatar][]">{{__('institute.please_choose')}}
                            </div>
                        </div>
                        <div id="owner_prev_holder_1" class="m-auto"
                             style="width: 80px;height:80px;">
                            <img src="{{ asset('defaults/no_preview.jpg') }}"
                                 class="img-thumbnail">
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group">
                        {!! Form::label('owners[name_en][]', __('institute.owner.name_en')) !!}
                        {!! Form::text('owners[name_en][]', old('owners[name_en][]'), ['class' => 'form-control', 'autofocus', 'id' => 'owners[name_en][]' ]) !!}
                    </div>
                </td>
                <td>
                    <div class="form-group text-right">
                        {!! Form::label('owners[name_ur][]', __('institute.owner.name_ur')) !!}
                        {!! Form::text('owners[name_ur][]', old('owners[name_ur][]'), ['class' => 'form-control', 'id' => 'owners[name_ur][]','dir'=>'rtl']) !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {!! Form::label('owners[phone][]', __('general.phone')) !!}
                        {!! Form::text('owners[phone][]', old('owners[phone][]'), ['class' => 'form-control', 'id' => 'owners[phone][]' ]) !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {!! Form::label('owners[cnic][]', __('general.cnic')) !!}
                        {!! Form::text('owners[cnic][]', old('owners[cnic][]'), ['class' => 'form-control', 'id' => 'owners[cnic][]' ]) !!}
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
