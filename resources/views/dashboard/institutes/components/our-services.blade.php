<div class="row">
    @if(count($services))
        @foreach($services as $service)
            <div class="col-md-4">
                <div class="form-group clearfix">
                    <div class="icheck-success d-inline">
                        <input type="checkbox" name="services[]" id="service_check_{{$service->id}}"
                               value="{{ $service->id }}">
                        <label for="service_check_{{$service->id}}">
                            @if(app()->getLocale()=='en')
                                {{ $service->name_en }}  <small class="text-info">({{ $service->slug }})</small>
                            @else
                                {{ $service->name_ur }}  <small class="text-info">({{ $service->slug }})</small>
                            @endif
                        </label>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>
