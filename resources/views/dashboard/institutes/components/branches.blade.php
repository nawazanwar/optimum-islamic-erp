<style>
    tbody:nth-child(odd) {
        background-color: #e9ebed29 !important;
    }
</style>
<div class="row">
    <div class="col-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <td colspan="10" class="text-right">
                    <div class="form-group mb-0 text-white">
                        <a class="btn btn-xs btn-success" onclick="Custom.addCloneTbody(this);"><i
                                class="fa fa-plus"></i></a>
                        <a class="btn btn-xs btn-danger" onclick="Custom.removeCloneTbody(this);"><i
                                class="fa fa-times"></i></a>
                    </div>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="2" class="text-center">
                    <div class="form-group">
                        {!! Form::label('branches[avatar]',__('institute.branch.choose_image')) !!}
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="branches[avatar][]"
                                       id="branches[avatar][]"
                                       onchange="Custom.preview(this,'image_holder')">
                                <label class="custom-file-label"
                                       for="branches[avatar][]">{{ __('institute.please_choose') }}</label>
                            </div>
                        </div>
                        <div class="p-3 text-center" id="image_holder">
                            <img height="200" width="150" class="img-thumbnail"
                                 src="{{ asset('defaults/no_preview.jpg') }}">
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group">
                        {!! Form::label('branches[name_en][]', __('institute.branch.name_en')) !!}
                        {!! Form::text('branches[name_en][]', old('branches[name_en][]'), ['class' => 'form-control', 'autofocus', 'id' => 'branches[name_en][]' ]) !!}
                    </div>
                </td>
                <td>
                    <div class="form-group text-right">
                        {!! Form::label('branches[name_ur][]', __('institute.branch.name_ur')) !!}
                        {!! Form::text('branches[name_ur][]', old('branches[name_ur][]'), ['class' => 'form-control', 'id' => 'branches[name_ur][]','dir'=>'rtl' ]) !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group text-right">
                        {!! Form::label('branches[location_en][]', __('institute.branch.location_en')) !!}
                        {!! Form::text('branches[location_en][]', old('branches[location_en][]'), ['class' => 'form-control', 'id' => 'branches[location_en][]']) !!}
                    </div>
                </td>
                <td>
                    <div class="form-group text-right">
                        {!! Form::label('branches[location_ur][]', __('institute.branch.location_ur')) !!}
                        {!! Form::text('branches[location_ur][]', old('branches[location_ur][]'), ['class' => 'form-control', 'id' => 'branches[location_ur][]','dir'=>'rtl']) !!}
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
