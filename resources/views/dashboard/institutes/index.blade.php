@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-header row align-items-center mx-1">
                    <div class="card-title col-xl-6 col-lg-6 col-md-6">
                        @can('create',\App\Models\Institute::class)
                            {!! link_to_route('institutes.create',__('institute.create'),null,['class'=>'btn bg-gradient-primary btn-sm']) !!}
                        @endcan
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 text-right">
                        <form action="{{ route('institutes.index') }}" method="get">
                            <div class="row justify-content-end">
                                <div class="col-xl-5">
                                    <select class="custom-select custom-select-sm" name="field">
                                        <option value="name" @if(isset($field) and $field == 'name') selected @endif>
                                            {{__('general.name')}}
                                        </option>
                                        <option value="location"
                                                @if(isset($field) and $field == 'location') selected @endif>
                                            {{__('general.location')}}
                                        </option>
                                    </select>
                                </div>
                                <div class="col-xl-5">
                                    <input type="search" name="keyword"
                                           value="@if(isset($keyword) and $keyword != '') {{ $keyword }} @endif"
                                           class="form-control form-control-sm"
                                           placeholder="{{__('btn.search_here')}}">
                                </div>
                                <div class="col-xl-2">
                                    <input type="submit" class="btn btn-info btn-sm" value="{{ __('btn.filter') }}">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body">
                    @include('partials.dashboard.message')
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{__('institute.name_en')}}</th>
                            <th>{{__('btn.active')}}</th>
                            <th class="text-center">{{__('institute.send_data')}}</th>
                            <th class="text-center">{{ __('institute.view_dashboard') }}</th>
                            <th class="text-center">{{__('general.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data)>0)
                            @foreach($data as $d)
                                <tr>
                                    <td class="text-center m-auto">
                                        <img class="img-circle img-thumbnail img-sm" src="{{$d->getAvatar()}}">
                                    </td>
                                    <td>{{$d->name_en}}</td>
                                    <td class="text-center">
                                        @include('dashboard.institutes.components.active-de-active',['data'=>$d])
                                    </td>
                                    <td class="text-center">
                                        @php
                                            $sync_route = $d->credentials[0]->domain_name."api/institute/sync/".$d->id;
                                            $sender_data = [
                                                'institute'=>[
                                                    'name_en'=>$d->name_en,
                                                    'name_ur'=>$d->name_ur,
                                                    'slug'=>$d->slug,
                                                    'location_en'=>$d->location_en,
                                                    'location_ur'=>$d->location_ur
                                                ]
                                             ];
                                            if ($d->services->count()>0){
                                                foreach ($d->services as $service){
                                                    $sender_data['services'][]=[
                                                      'id'=>$service->id,
                                                      'name_en'=>$service->name_en,
                                                      'name_ur'=>$service->name_ur,
                                                      'slug'=>$service->slug,
                                                      'is_sync'=>$service->pivot->is_sync
                                                    ];
                                                }
                                            }

                                            if ($d->branches->count()>0){
                                                foreach ($d->branches as $branch){
                                                    $sender_data['branches'][]=[
                                                        'id'=>$branch->id,
                                                        'name_en'=>$branch->name_en,
                                                        'name_ur'=>$branch->name_ur,
                                                        'location_en'=>$branch->location_en,
                                                        'location_ur'=>$branch->location_ur,
                                                        'slug'=>$branch->slug,
                                                        'active'=>$branch->active
                                                    ];
                                                }
                                            }

                                            $sender_data = json_encode($sender_data);
                                        @endphp
                                        <a class="btn btn-success btn-sm text-white"
                                           onclick="startSync('{{$sync_route}}','{{ $sender_data }}')">{{__('institute.start_sending')}}
                                            <i class="fa fa-sync"></i></a>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('institute-manage-dashboard.index',['pId'=>$d->id]) }}"
                                           class="btn btn-info btn-sm text-white">{{__('institute.start_viewing')}}
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <button type="button"
                                                    class="btn btn-default">{{__('general.action')}}</button>
                                            <button type="button" class="btn btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                @can('read',\App\Models\Institute::class)
                                                    <li class="dropdown-item">{!! link_to_route('institutes.show', __('btn.show'), [$d->id]) !!}</li>
                                                @endif
                                                @can('edit',\App\Models\Institute::class)
                                                    <li class="dropdown-item">{!! link_to_route('institutes.edit', __('btn.edit'), [$d->id]) !!}</li>
                                                @endif
                                                @can('delete',\App\Models\Institute::class)
                                                    <form action="{{ route('institutes.destroy',$d->id) }}"
                                                          method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <li class="dropdown-item">
                                                            <button type="submit" class="bg-transparent"
                                                                    onclick="return confirm({{__('message.dou_you_really_wants_to_delete')}})"
                                                                    style="border: none;padding-left: 0;color: #c13535;">
                                                                {{__('btn.delete')}}
                                                            </button>
                                                        </li>
                                                    </form>
                                                @endcan
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="12" class="p-3">{{__('message.no_record_found')}}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix">
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptInnerFiles')

    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
@stop
@section('pageScript')
    <script>
        function startSync(route, data) {
            data = JSON.parse(data);
            CrossAjax.call(route, data, 'POST', function (response) {
                if (response.status == 200) {
                    toastr.success("{{ __('institute.message.sync_successfully') }}")
                }
            });
        }
    </script>
@endsection
