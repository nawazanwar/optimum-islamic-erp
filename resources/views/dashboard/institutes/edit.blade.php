@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop
@section('content')
    <div class="card card-solid">
        @can('read',\App\Models\Institute::class)
            <div class="card-header">
                <div class="card-title">
                    {!! link_to_route('institutes.index',__('institute.title'),null,['class'=>'btn btn-info btn-sm']) !!}
                </div>
            </div>
        @endcan
        <div class="card-body pb-0">
            {!! Form::model($model, ['files' => true,'onsubmit'=>'return false;',"id"=>"updateForm"] ) !!}
            {!! csrf_field() !!}
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('name', __('general.name_en')) !!}
                                {!! Form::text('name', old('name'), ['class' => 'form-control', 'autofocus','placeholder'=>__('placeholder.name_en'), 'id' => 'name' ]) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                {!! Form::label('name_ur', __('general.name_ur')) !!}
                                {!! Form::text('name_ur', old('name_ur'), ['class' => 'form-control', 'autofocus','placeholder'=>__('placeholder.name_ur'), 'id' => 'institute_name_ur','dir'=>'rtl']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('location', __('general.location')) !!}
                                {!! Form::text('location', old('location'), ['class' => 'form-control', 'id' => 'location','placeholder'=>__('placeholder.location') ]) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('avatar',__('general.choose_image')) !!}
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="avatar" id="avatar"
                                       onchange="Custom.preview(this,'image_holder')">
                                <label class="custom-file-label" for="avatar">Choose Image</label>
                            </div>
                        </div>
                        <div class="p-3 text-center" id="image_holder">
                            <img height="200" width="150" class="img-thumbnail"
                                 src="{{ $model->avatar() }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('institute.detail_of_owners') }}</h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered">
                                @isset($model->owners)
                                    @foreach($model->owners as $key=>$owner)
                                        <tr>
                                            <td>
                                                {!! Form::hidden('owners[id][]',$owner->id) !!}
                                                <div class="form-group">
                                                    {!! Form::label('owners[avatar][]',__('general.choose_image')) !!}
                                                    <div class="custom-file mb-2">
                                                        <input type="file" class="custom-file-input"
                                                               name="owners[avatar][]"
                                                               onchange="Custom.preview(this,'owner_prev_holder_{{$owner->id}}')"
                                                               id="owners[avatar][{{ $key }}]">
                                                        <div class="custom-file-label" for='owners[avatar][{{ $key }}]'>
                                                            Choose
                                                            Image
                                                        </div>
                                                    </div>
                                                    <div id="owner_prev_holder_{{$owner->id}}" class="m-auto"
                                                         style="width: 80px;height:80px;">
                                                        <img src="{{ $owner->avatar() }}"
                                                             class="img-thumbnail">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    {!! Form::label('owners[name]['.$key.']', __('general.name_en')) !!}
                                                    {!! Form::text('owners[name][]', old('owners[name][]',$owner->name), ['class' => 'form-control', 'autofocus','placeholder'=>__('placeholder.name_en'), 'id' => 'owners[name]['.$key.']' ]) !!}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    {!! Form::label('owners[name_ur]['.$key.']', __('general.name_ur')) !!}
                                                    {!! Form::text('owners[name_ur][]', old('owners[name_ur][]',$owner->name_ur), ['class' => 'form-control', 'autofocus','placeholder'=>__('placeholder.name_ur'), 'id' => 'owners[name_ur]['.$key.']' ]) !!}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group text-center mb-0 text-white">
                                                    <a class="btn btn-xs btn-success"
                                                       onclick="Custom.addClone(this);"><i
                                                            class="fa fa-plus"></i></a>
                                                    <a class="btn btn-xs btn-danger"
                                                       onclick="Custom.removeClone(this);"><i
                                                            class="fa fa-times"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                {!! Form::label('owners[avatar][]',__('general.choose_image')) !!}
                                                <div class="custom-file mb-2">
                                                    <input type="file" class="custom-file-input"
                                                           name="owners[avatar][]"
                                                           onchange="Custom.preview(this,'owner_prev_holder_1')"
                                                           id="owners[avatar][]">
                                                    <div class="custom-file-label" for="owners[avatar][]">Choose Image
                                                    </div>
                                                </div>
                                                <div id="owner_prev_holder_1" class="m-auto"
                                                     style="width: 80px;height:80px;">
                                                    <img src="{{ asset('defaults/no_preview.jpg') }}"
                                                         class="img-thumbnail">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                {!! Form::label('owners[name][]', __('general.name_en')) !!}
                                                {!! Form::text('owners[name][]', old('owners[name][]'), ['class' => 'form-control', 'autofocus','placeholder'=>__('placeholder.name_en'), 'id' => 'owners[name][]' ]) !!}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                {!! Form::label('owners[name_ur][]', __('general.name_ur')) !!}
                                                {!! Form::text('owners[name_ur][]', old('owners[name_ur][]'), ['class' => 'form-control', 'autofocus','placeholder'=>__('placeholder.name_ur'), 'id' => 'owners[name_ur][]' ]) !!}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group text-center mb-0 text-white">
                                                <a class="btn btn-xs btn-success" onclick="Custom.addClone(this);"><i
                                                        class="fa fa-plus"></i></a>
                                                <a class="btn btn-xs btn-danger" onclick="Custom.removeClone(this);"><i
                                                        class="fa fa-times"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endisset
                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group text-right">
                                        {!! Form::submit(__('btn.save'), array('class' => 'btn btn-primary pull-left btn-sm')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
@stop
@section('scriptInnerFiles')
    <script type="text/javascript"
            src="{{ asset('plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>
@stop
@section('pageScript')

    <script type="text/javascript"
            src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyC4KM8oF8-EwHa66m3N6h4dS-zb0mVtVx4"></script>
    <script>

        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('location'));
            google.maps.event.addListener(places, 'location');
        });
        (function ($) {
            $("#updateForm").validate({
                rules: {
                    name: "required",
                    name_ur: "required",
                    location: "required",
                    "owners[name][]": 'required',
                    "owners[name_ur][]": 'required'
                }, submitHandler: function (form) {
                    Ajax.setAjaxHeader();
                    var formData = new FormData(form);
                    Ajax.call("{{ route('institutes.edit',[$model]) }}", formData, 'POST', function (response) {
                        if (response.status == 200) {
                            toastr.success(response.message);
                        }
                    })
                }
            });
        })(jQuery)

    </script>
@endsection
