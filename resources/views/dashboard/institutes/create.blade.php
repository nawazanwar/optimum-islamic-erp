@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/multiform/css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/multiform/css/dots.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
@endsection

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop
@section('content')
    <form action="{{ route('institutes.store')  }}" method="post" id="createForm" onsubmit="return false;"
          enctype="multipart/form-data">
        @csrf
        <div class="card card-solid">
            @can('read',\App\Models\Institute::class)
                <div class="card-header">
                    <div class="card-title">
                        {!! link_to_route('institutes.index',__('institute.title'),null,['class'=>'btn btn-info btn-sm']) !!}
                    </div>
                </div>
            @endcan
            <div class="card-body">
                <div id="dotwizard">
                    <ul>
                        <li>
                            <a href="#general_detail">1<br/>
                                <span class="fs-15">
                                {{ __('institute.detail') }}
                            </span>
                            </a>
                        </li>
                        <li>
                            <a href="#owner_detail">2<br/>
                                <span class="fs-15">
                                {{ __('institute.owners_detail') }}
                            </span>
                            </a>
                        </li>
                        <li>
                            <a href="#branch_detail">2<br/>
                                <span class="fs-15">
                                {{ __('institute.branches') }}
                            </span>
                            </a>
                        </li>
                        <li>
                            <a href="#selected_services">3<br/>
                                <span class="fs-15">
                                {{ __('institute.our_services') }}
                            </span>
                            </a>
                        </li>
                        <li>
                            <a href="#extra_credentials">4<br/>
                                <span class="fs-15">
                                {{ __('institute.domain_hosting_info') }}
                            </span>
                            </a>
                        </li>
                    </ul>
                    <div class="mt-4">
                        <div id="general_detail">
                            @include('dashboard.institutes.components.general-detail')
                        </div>
                        <div id="owner_detail">
                            @include('dashboard.institutes.components.owner-details')
                        </div>
                        <div id="branch_detail">
                            @include('dashboard.institutes.components.branches')
                        </div>
                        <div id="selected_services">
                            @include('dashboard.institutes.components.our-services')
                        </div>
                        <div id="extra_credentials">
                            @include('dashboard.institutes.components.credentials')
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                {!! Form::submit(__('btn.save'), array('class' => 'btn btn-primary')) !!}
            </div>
        </div>
    </form>
@stop
@section('scriptInnerFiles')
    <script type="text/javascript"
            src="{{ asset('plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('plugins/multiform/js/style.min.js') }}"></script>
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
@stop
@section('pageScript')
    <script>
        (function ($) {

            $('#dotwizard').smartWizard({
                selected: 0,
                theme: 'dots',
                autoAdjustHeight: true,
                transitionEffect: 'fade',
                showStepURLhash: false,
            });

            $("#createForm").validate({
                rules: {
                    "institute[name_en]": "required",
                    "institute[name_ur]": "required"
                }, messages: {
                    "institute[name_en]": {
                        required: function () {
                            toastr.error("{{ __('institute.message.name_en') }}")
                        },
                    },
                    "institute[name_ur]": {
                        required: function () {
                            toastr.error("{{ __('institute.message.name_ur') }}")
                        },
                    }
                }, submitHandler: function (form) {
                    Ajax.setAjaxHeader();
                    var formData = new FormData(form);
                    Ajax.call("{{ route('institutes.store') }}", formData, 'POST', function (response) {
                        if (response.status == 409) {
                            toastr.error(response.message);
                        } else {
                            if (response.status == 200) {
                                toastr.success(response.message);
                                window.location.assign("{{ route('institutes.index') }}");
                            }
                        }
                    })
                }
            });
        })(jQuery)

    </script>
@endsection
