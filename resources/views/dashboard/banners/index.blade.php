@extends('layouts.dashboard')
@section('pageTitle', $pageTitle)

@section('breadcrumbs')
    @include('partials.dashboard.breadcrumbs', ['pageTitle' => $pageTitle,'breadcrumbs' => $breadcrumbs])
@stop
@section('styleInnerFiles')
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('content')
    <style>
        tbody:nth-child(odd) {
            background-color: #e9ebed29 !important;
        }
    </style>
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="card">
                <div class="card-body">
                    @include('partials.dashboard.message')
                    {!! Form::open(['route' => 'banners.store', 'files' => true] ) !!}
                    {!! csrf_field() !!}
                    <table class="table table-bordered table-condenced">
                        <thead>
                        <tr>
                            <td colspan="3">
                                <div class="row">
                                    <div class="col-3">
                                        {!! Form::submit(__('btn.save'), array('class' => 'btn btn-primary')) !!}
                                    </div>
                                    <div class="col-9 text-right">
                                        <a class="btn btn-primary btn-xs text-white"
                                           onclick="Custom.addCloneTbody(this);return false;"><i
                                                class="fa fa-plus"></i></a>
                                        <a class="btn btn-danger btn-xs text-white"
                                           onclick="Custom.removeCloneTbody(this);return false;"><i
                                                class="fa fa-minus"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </thead>
                        @if(count($data)>0)
                            @foreach($data as $d)
                                <tbody>
                                <tr>
                                    <td colspan="2">
                                        {!! Form::hidden('id[]',$d->id,['class'=>'id_field']) !!}
                                        <div class="form-group">
                                            {!! Form::label('image',__('banner.choose_bg_image')) !!}
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="image[]"
                                                           id="image"
                                                           onchange="Custom.preview(this,'image_holder')">
                                                    <label class="custom-file-label" for="avatar">Choose Image</label>
                                                </div>
                                            </div>
                                            <div class="p-3 text-center" id="image_holder">
                                                <img height="200" width="150" class="img-thumbnail"
                                                     src="{{ $d->image() }}">
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="1">
                                        <div class="form-group">
                                            {!! Form::label('avatar',__('banner.choose_for_image')) !!}
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="avatar[]"
                                                           id="avatar"
                                                           onchange="Custom.preview(this,'avatar_holder')">
                                                    <label class="custom-file-label" for="avatar">Choose Image</label>
                                                </div>
                                            </div>
                                            <div class="p-3 text-center" id="avatar_holder">
                                                <img height="200" width="150" class="img-thumbnail"
                                                     src="{{ $d->avatar() }}">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {!! Form::label('heading_'.$d->id,__('banner.heading')) !!}
                                        <textarea type="text" name="heading[]" class="form-control"
                                                  placeholder="{{ __('banner.placeholders.heading') }}" rows="2"
                                                  id="heading_{{$d->id}}">{{ $d->heading }}</textarea>
                                    </td>
                                    <td class="text-right">
                                        {!! Form::label('heading_ur_'.$d->id,__('banner.heading_ur')) !!}
                                        <textarea type="text" name="heading_ur[]" class="form-control" dir="rtl"
                                                  placeholder="{{ __('banner.placeholders.heading_ur') }}" rows="2"
                                                  id="heading_ur_{{$d->id}}">{{ $d->heading_ur }}</textarea>
                                    </td>
                                    <td class="text-center">
                                        <div class="icheck-success d-inline">
                                            <input type="checkbox" value="{{ $d->active?true:false }}"
                                                   onchange="Custom.manage_check_box_value(this);"
                                                   @if($d->active) checked @endif name="active[]"
                                                   id="active-{{$d->id}}">
                                            <label for="active-{{$d->id}}"></label>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            @endforeach
                        @endif
                    </table>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
