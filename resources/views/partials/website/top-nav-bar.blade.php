<div class="main-header hidden-sm-down" id="sticky">
    <div class="container">
        <div class="row" style="height: 80px;">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="logo-area">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('website/images/logo/logo.png') }}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="menu-area">
                    <nav>
                        <ul class="list-inline">
                            <li><a href="{{ route('home') }}">{{__('nav.home')}}</a></li>
                            <li class="active"><a href="contact.html">{{__('nav.about-us')}}</a></li>
                            <li><a href="contact.html">{{__('nav.contact-us')}}</a></li>
                            <li><a href="contact.html">{{__('nav.e-learning')}}</a></li>
                            <li><a href="contact.html">{{__('nav.latest-courses')}}</a></li>
                            <li><a href="contact.html">{{__('nav.latest-news-and-events')}}</a></li>
                            <li class="menu-btn">
                                <ul>
                                    <li><span class="search-ico"><i class="flaticon-magnifying-glass"></i></span>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="search-box">
                    <form>
                        <input type="search" placeholder="">
                        <button><i class="flaticon-magnifying-glass"></i></button>
                        <span class="close-search"><i class="flaticon-cancel"></i></span>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mobile-menu-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mobile-menu">
                    <nav id="dropdown" style="display: block;">
                        <ul class="list-inline">
                            <li><a href="index.html">Home</a></li>
                            <li class="drop-menu">
                                <a href="javascript:void(0)">Causes</a>
                                <ul class="down-menu">
                                    <li><a href="causes-grid.html">causes grid</a></li>
                                    <li><a href="causes-list.html">causes list</a></li>
                                    <li><a href="causes-single.html">causes single</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Event</a></li>
                            <li class="drop-menu">
                                <a href="javascript:void(0)">Pages</a>
                                <ul class="down-menu">
                                    <li><a href="about.html">about</a></li>
                                    <li><a href="blog.html">blog</a></li>
                                    <li><a href="blog-single.html">blog single</a></li>
                                    <li><a href="causes-grid.html">causes grid</a></li>
                                    <li><a href="causes-list.html">causes list</a></li>
                                    <li><a href="causes-single.html">causes single</a></li>
                                    <li><a href="event.html">event</a></li>
                                    <li><a href="gallery-col-2.html">gallery-col-2</a></li>
                                    <li><a href="gallery-col-3.html">gallery-col-3</a></li>
                                    <li><a href="gallery-col-4.html">gallery-col-4</a></li>
                                    <li><a href="contact.html">contact</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Sermons</a></li>
                            <li class="drop-menu">
                                <a href="javascript:void(0)">Blog</a>
                                <ul class="down-menu">
                                    <li><a href="blog.html">blog</a></li>
                                    <li><a href="blog-single.html">blog single</a></li>
                                </ul>
                            </li>
                            <li><a href="contact.html">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
