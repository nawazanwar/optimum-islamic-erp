<style>
    .list-inline {
        float: left;
        margin: 7px 9px;
    }
</style>
<header>
    <div class="topbar hidden-sm-down">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="header-event">
                        <ul class="list-inline count-list">
                            <li><h6>Next Event:</h6></li>
                            <li><span class="count days">02</span><span class="con">D</span></li>
                            <li><span class="count hours">23</span><span class="con">H</span></li>
                            <li><span class="count minutes">57</span><span class="con">M</span></li>
                            <li><span class="count seconds">36</span><span class="con">S</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <div class="header-social">
                        <ul class="list-inline">
                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    @if(auth()->user())
                        <ul class="list-inline count-list btn-theme">
                            <li class="nav-item dropdown m-2 p-1">
                                <a class="dropdown-toggle text-white" href="#" id="navbardrop"
                                   data-toggle="dropdown">
                                    {{ auth()->user()->name }}
                                </a>
                                <div class="dropdown-menu mt-3">
                                    <a class="dropdown-item"
                                       href="{{ route('dashboard') }}">{{ __('btn.dashboard') }}</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        {{ __('btn.logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        </ul>
                    @else
                        <ul class="list-inline count-list btn-theme">
                            <li class="nav-item dropdown m-2 p-1 mx-1">
                                <a class="text-white" href="{{ route('login') }}">
                                    {{ __('btn.login') }}
                                </a>
                            </li>
                            <li class="nav-item dropdown m-2 p-1 mx-1">
                                <a class="text-white" href="{{ route('register') }}">
                                    {{ __('btn.register') }}
                                </a>
                            </li>
                        </ul>
                    @endif
                    <ul class="list-inline count-list btn-theme">
                        <li class="nav-item dropdown m-2 p-1">
                            <a class="dropdown-toggle text-white" href="#" id="navbardrop"
                               data-toggle="dropdown">
                                {{(app()->getLocale()=="ur")?'اردو':'English'}}
                            </a>
                            <div class="dropdown-menu mt-3">
                                <a class="dropdown-item" href="{{route('lang.switcher',['ur'])}}">اردو</a>
                                <a class="dropdown-item" href="{{route('lang.switcher',['en'])}}">English</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @include('partials.website.top-nav-bar')
</header>
