<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{ asset('dashboard/img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('dashboard/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                     alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('notifications.index') }}" class="nav-link @if(strpos(request()->url(), 'notifications')) {{ 'active' }} @endif">
                        <i class="nav-icon fa fa-bell"></i>
                        <p>
                            {{ __('dashboard.notifications') }}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('services.index') }}" class="nav-link @if(strpos(request()->url(), 'services')) {{ 'active' }} @endif">
                        <i class="nav-icon fas fa-file-image"></i>
                        <p>
                            {{ __('dashboard.services') }}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('institutes.index') }}" class="nav-link @if(strpos(request()->url(), 'institutes')) {{ 'active' }} @endif">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            {{ __('dashboard.institutes') }}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('banners.index') }}" class="nav-link @if(strpos(request()->url(), 'banners')) {{ 'active' }} @endif">
                        <i class="nav-icon fas fa-sliders-h"></i>
                        <p>
                            {{ __('dashboard.banners') }}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('albums.index') }}" class="nav-link @if(strpos(request()->url(), 'albums')) {{ 'active' }} @endif">
                        <i class="nav-icon fa fa-image" aria-hidden="true"></i>
                        <p>
                            {{ __('dashboard.albums') }}
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
