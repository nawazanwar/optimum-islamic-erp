@if($errors->any())
    <div class="card-body">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-ban"></i> Alert!</h5>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

@if ($successMessage = \Illuminate\Support\Facades\Session::get('successMessage'))
    <div class="card-body">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i> {{__('system.alert')}}</h5>
            {{$successMessage}}
        </div>
    </div>
@endif

@if ($errorMessage = \Illuminate\Support\Facades\Session::get('errorMessage'))
    <div class="card-body">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5>{{__('system.alert')}}</h5>
            {{$errorMessage}}
        </div>
    </div>
@endif
