<section class="subscribe-area section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="subs-form">
                    <form>
                        <h2><span><img src="{{ asset('website/css/images/icon/msg.png') }} alt=""/></span><span class="con pd-t5">Sign up for Newsletter</span>
                        </h2>
                        <fieldset>
                            <input type="email" placeholder="Email Address...">
                            <span><i class="fa fa-envelope"></i></span>
                        </fieldset>

                        <button type="submit" class="btn3"><span>Submit</span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
