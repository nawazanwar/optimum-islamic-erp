<section class="videos-area-one section">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                <div class="video-thumb">
                    <figure>
                        <div class="thumbnail">
                            <img src="{{ asset('website/images/video/1.jpg') }}" alt=""/>
                            <a href="https://www.youtube.com/watch?v=0I8GmbDU7c4" class="play-ico video-play-icon"></a>
                        </div>
                        <div class="con"><h4>WATCH Our Video</h4></div>
                    </figure>
                </div>
            </div>
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <div class="video-content">
                    <h2>In The Name Of Allah <br>The Beneficent The Merciful</h2>
                    <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin. This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean itudin. This is Photoshop's sion  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. This is Photoshop's versionf Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin,</p>
                    <div class="about">
                        <div class="w-50 floatleft pd-r15">
                            <h4><span><i class="fa fa-eye"></i></span> Our Vision</h4>
                            <p>Nibh vel velit auctor aliquet. Aenean ituin. This is Photoshop's version  of Phooshop's sion  of Lorem.</p>
                        </div>
                        <div class="w-50 floatleft pd-l15">
                            <h4><span><i class="fa fa-flag"></i></span> Our Mission</h4>
                            <p>Nibh vel velit auctor aliquet. Aenean ituin. This is Photoshop's version  of Phooshop's sion  of Lorem.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
