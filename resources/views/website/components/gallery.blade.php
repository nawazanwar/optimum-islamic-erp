<section class="gallery-area section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="section-heading-two">
                    <h2>Our Photo Gallery</h2>
                    <p>Denean sollicitudin. This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit
                        auctor aliquet. Aenean itudin. This is Photoshop's sion of Lorem Ipsum. Proin gravida nibh vel
                        velit.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="gallery-nav text-center">
                    <ul class="list-inline">
                        <li class="filter" data-filter="all">All</li>
                        <li class="filter" data-filter=".ashura">Ashura</li>
                        <li class="filter" data-filter=".azha">Eud Ul Azha</li>
                        <li class="filter" data-filter=".fitr">Eid Ul Fitr</li>
                        <li class="filter" data-filter=".milad">Milad Un Nabi</li>
                        <li class="filter" data-filter=".ramdan">Ramdan</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row" id="Container">
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 mix fitr">
                <div class="gallery">
                    <figure>
                        <a href="{{ asset('website/images/gallery/1.jpg') }}">
                            <img src="{{ asset('website/images/gallery/1.jpg') }}" alt=""/>
                            <span></span>
                        </a>
                    </figure>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12 mix ashura milad">
                <div class="gallery">
                    <figure>
                        <a href="{{ asset('website/images/gallery/2.jpg') }}">
                            <img src="{{ asset('website/images/gallery/2.jpg') }}" alt=""/>
                            <span></span>
                        </a>
                    </figure>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12 mix fitr azha">
                <div class="gallery">
                    <figure>
                        <a href="{{ asset('website/images/gallery/3.jpg') }}">
                            <img src="{{ asset('website/images/gallery/3.jpg') }}" alt=""/>
                            <span></span>
                        </a>
                    </figure>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12 mix">
                <div class="row">
                    <div class="col-sm-12 mix milad ramdan">
                        <div class="gallery">
                            <figure>
                                <a href="{{ asset('website/images/gallery/4.jpg') }}">
                                    <img src="{{ asset('website/images/gallery/4.jpg') }}" alt=""/>
                                    <span></span>
                                </a>
                            </figure>
                        </div>
                    </div>
                    <div class="col-sm-12 mix ramdan">
                        <div class="gallery">
                            <figure>
                                <a href="{{ asset('website/images/gallery/7.jpg') }}">
                                    <img src="{{ asset('website/images/gallery/7.jpg') }}" alt=""/>
                                    <span></span>
                                </a>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 mix milad azha">
                <div class="gallery">
                    <figure>
                        <a href="{{ asset('website/images/gallery/5.jpg') }}">
                            <img src="{{ asset('website/images/gallery/5.jpg') }}" alt=""/>
                            <span></span>
                        </a>
                    </figure>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12 mix ramdan ashura">
                <div class="gallery">
                    <figure>
                        <a href="{{ asset('website/images/gallery/6.jpg') }}">
                            <img src="{{ asset('website/images/gallery/6.jpg') }}" alt=""/>
                            <span></span>
                        </a>
                    </figure>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="gallery-load text-center mr-t25">
                    <a href="#" class="btn4"><span>View More</span></a>
                </div>
            </div>
        </div>
    </div>
</section>
