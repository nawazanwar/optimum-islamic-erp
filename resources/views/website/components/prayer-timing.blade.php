<section class="banner-area-two section2 bg-img jarallax af">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="section-heading-one">
                    <h2>Prayer Timings</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="prayer-list text-center mr-b30">
                    <h4>Dawn Prayer</h4>
                    <strong>Fajr</strong>
                    <button>4:01 am</button>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="prayer-list text-center mr-b30">
                    <h4>Sunrise Time</h4>
                    <strong>Zohar</strong>
                    <button>1:30 AM</button>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="prayer-list text-center mr-b30">
                    <h4>Afternoon</h4>
                    <strong>Asar</strong>
                    <button>5:30 PM</button>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="prayer-list text-center mr-b30">
                    <h4>Sunset Prayer</h4>
                    <strong>Magrib</strong>
                    <button>7:15 PM</button>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="prayer-list text-center mr-b30">
                    <h4>Evening Prayer</h4>
                    <strong>Isha</strong>
                    <button>9:00 PM</button>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <div class="prayer-list text-center mr-b30">
                    <h4>Sunrise Time</h4>
                    <strong>Kudba</strong>
                    <button>2:00 PM</button>
                </div>
            </div>
        </div>
    </div>
</section>
