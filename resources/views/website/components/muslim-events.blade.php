<section class="event-area section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="section-heading-two">
                    <h2>Muslim Events</h2>
                    <p>Denean sollicitudin. This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit
                        auctor aliquet. Aenean itudin. This is Photoshop's sion of Lorem Ipsum. Proin gravida nibh vel
                        velit.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid pd-0">
        <div class="event-slider">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 pd-l0">
                    <div class="event-list-left">
                        <div class="row">
                            <div class="col-sm-7 col-xs-12 pd-0">
                                <div class="content">
                                    <span class="date">21 July 2017</span>
                                    <h4>Learn Hajj Rituals, Manajaat</h4>
                                    <ul class="list-inline">
                                        <li><i class="fa fa-clock-o"></i> 10:00 Am to 12:00 Pm</li>
                                        <li><i class="fa fa-map-marker"></i> Mosque, Makkah</li>
                                    </ul>
                                    <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin. This is
                                        Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                                        Aenean Morbi. <a href="#">Read More</a></p>
                                    <a href="#" class="btn4"><span>Join Us!</span></a>
                                </div>
                            </div>
                            <div class="col-sm-5 col-xs-12 pd-l0">
                                <div class="event-img">
                                    <figure>
                                        <img src="{{ asset('website/images/events/1.jpg') }}" alt=""/>
                                        <ul class="count-list list-inline">
                                            <li><span class="count days">00</span>
                                                <p>DAYS</p></li>
                                            <li><span class="count hours">00</span>
                                                <p>hours</p></li>
                                            <li><span class="count minutes">00</span>
                                                <p>minutes</p></li>
                                            <li><span class="count seconds">00</span>
                                                <p>seconds</p></li>
                                        </ul>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 pd-r0">
                    <div class="event-list-right">
                        <div class="row">
                            <div class="col-sm-5 col-xs-12 pd-r0">
                                <div class="event-img">
                                    <figure>
                                        <img src="{{ asset('website/images/events/2.jpg') }}" alt=""/>
                                        <ul class="count-list list-inline">
                                            <li><span class="count days">00</span>
                                                <p>DAYS</p></li>
                                            <li><span class="count hours">00</span>
                                                <p>hours</p></li>
                                            <li><span class="count minutes">00</span>
                                                <p>minutes</p></li>
                                            <li><span class="count seconds">00</span>
                                                <p>seconds</p></li>
                                        </ul>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-7 col-xs-12 pd-0">
                                <div class="content">
                                    <span class="date">21 July 2017</span>
                                    <h4>Learn Hajj Rituals, Manajaat</h4>
                                    <ul class="list-inline">
                                        <li><i class="fa fa-clock-o"></i> 10:00 Am to 12:00 Pm</li>
                                        <li><i class="fa fa-map-marker"></i> Mosque, Makkah</li>
                                    </ul>
                                    <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin. This is
                                        Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                                        Aenean Morbi. <a href="#">Read More</a></p>
                                    <a href="#" class="btn4"><span>Join Us!</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 pd-l0">
                    <div class="event-list-left">
                        <div class="row">
                            <div class="col-sm-7 col-xs-12 pd-0">
                                <div class="content">
                                    <span class="date">21 July 2017</span>
                                    <h4>Learn Hajj Rituals, Manajaat</h4>
                                    <ul class="list-inline">
                                        <li><i class="fa fa-clock-o"></i> 10:00 Am to 12:00 Pm</li>
                                        <li><i class="fa fa-map-marker"></i> Mosque, Makkah</li>
                                    </ul>
                                    <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin. This is
                                        Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                                        Aenean Morbi. <a href="#">Read More</a></p>
                                    <a href="#" class="btn4"><span>Join Us!</span></a>
                                </div>
                            </div>
                            <div class="col-sm-5 col-xs-12 pd-l0">
                                <div class="event-img">
                                    <figure>
                                        <img src="{{ asset('website/images/events/1.jpg') }}" alt=""/>
                                        <ul class="count-list list-inline">
                                            <li><span class="count days">00</span>
                                                <p>DAYS</p></li>
                                            <li><span class="count hours">00</span>
                                                <p>hours</p></li>
                                            <li><span class="count minutes">00</span>
                                                <p>minutes</p></li>
                                            <li><span class="count seconds">00</span>
                                                <p>seconds</p></li>
                                        </ul>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 pd-r0">
                    <div class="event-list-right">
                        <div class="row">
                            <div class="col-sm-5 col-xs-12 pd-r0">
                                <div class="event-img">
                                    <figure>
                                        <img src="{{ asset('website/images/events/2.jpg') }}" alt=""/>
                                        <ul class="count-list list-inline">
                                            <li><span class="count days">00</span>
                                                <p>DAYS</p></li>
                                            <li><span class="count hours">00</span>
                                                <p>hours</p></li>
                                            <li><span class="count minutes">00</span>
                                                <p>minutes</p></li>
                                            <li><span class="count seconds">00</span>
                                                <p>seconds</p></li>
                                        </ul>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-7 col-xs-12 pd-0">
                                <div class="content">
                                    <span class="date">21 July 2017</span>
                                    <h4>Learn Hajj Rituals, Manajaat</h4>
                                    <ul class="list-inline">
                                        <li><i class="fa fa-clock-o"></i> 10:00 Am to 12:00 Pm</li>
                                        <li><i class="fa fa-map-marker"></i> Mosque, Makkah</li>
                                    </ul>
                                    <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin. This is
                                        Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                                        Aenean Morbi. <a href="#">Read More</a></p>
                                    <a href="#" class="btn4"><span>Join Us!</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
