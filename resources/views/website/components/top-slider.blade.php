<!--Slider area start here-->
<section class="slider-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 msj_slider_padding">
                <div id="carouselExampleIndicators" class="slide carousel" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <img src="{{ asset('website/images/slider/1.jpg') }}" alt=""/>
                            <div class="carousel-caption one">
                                <div class="content">
                                    <img class="animated fadeInDown" src="{{ asset('website/images/slider/sm-1.png') }}"
                                         alt=""/>
                                    <h2 class="animated fadeInDown"><span class="primary">Allah</span> Help Those<br>
                                        Who Help <span class="hover">Themselves</span></h2>
                                    <a href="#" class="btn2 animated fadeInUp"><span>Learn More</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('website/images/slider/2.jpg') }}" alt=""/>
                            <div class="carousel-caption one">
                                <div class="content">
                                    <img class="animated fadeInDown" src="{{ asset('website/images/slider/sm-1.png') }}"
                                         alt=""/>
                                    <h2 class="animated fadeInDown"><span class="primary">In The</span> Name Of
                                        Allah<br>The Beneficent<span class="hover">The Merciful</span></h2>
                                    <a href="#" class="btn2 animated fadeInUp"><span>Learn More</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev carousel-control" href="#carouselExampleIndicators" role="button"
                       data-slide="prev"><i class="flaticon-back"></i></a>
                    <a class="carousel-control-next carousel-control" href="#carouselExampleIndicators" role="button"
                       data-slide="next"><i class="flaticon-next"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
