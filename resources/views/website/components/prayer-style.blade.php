<section class="prayer-area pd-t100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="section-heading-two">
                    <h2>Islamic Prayer</h2>
                    <p>Denean sollicitudin. This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit
                        auctor aliquet. Aenean itudin. This is Photoshop's sion of Lorem Ipsum. Proin gravida nibh vel
                        velit.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="prayer-img">
                    <img src="{{ asset('website/images/banner/bg-2.jpg') }}" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-md-down">
                <div class="info">
                    <div class="sun">
                        <span class="mid"><img src="{{ asset('website/css/images/icon/1.png') }}" alt=""></span>
                        <span class="ico1"><strong>Kalma</strong></span>
                        <span class="ico2"><strong>Salat</strong></span>
                        <span class="ico3"><strong>Fasting</strong></span>
                        <span class="ico4"><strong>Zakaat</strong></span>
                        <span class="ico5"><strong>Hajj</strong></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
