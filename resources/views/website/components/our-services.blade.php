<section class="service-area pd-t100" style="background-image: url({{ asset('website/images/services/1.png') }})">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="section-heading-two">
                    <h2>Our Services</h2>
                    <p>Denean sollicitudin. This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit
                        auctor
                        aliquet. Aenean itudin. This is Photoshop's sion of Lorem Ipsum. Proin gravida nibh vel
                        velit.</p>
                </div>
            </div>
        </div>
        <div class="row pd-b100 pd-t70">
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="service-left">
                    <div class="service-list">
                        <h4>Parent Education<span><i class="fa fa-book"></i></span></h4>
                        <p>Nibh vel velit auctor aliquet. Aenean ituin This is Photoshop's version .</p>
                    </div>
                    <div class="service-list">
                        <h4>Video Sermons<span><i class="fa fa-video-camera"></i></span></h4>
                        <p>Nibh vel velit auctor aliquet. Aenean ituin This is Photoshop's version .</p>
                    </div>
                    <div class="service-list">
                        <h4>Find a Mosk<span><i class="fa fa-home"></i></span></h4>
                        <p>Nibh vel velit auctor aliquet. Aenean ituin This is Photoshop's version .</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 offset-lg-4">
                <div class="service-right">
                    <div class="service-list">
                        <h4><span><i class="fa fa-eye"></i></span>Hadith & Sunnah</h4>
                        <p>Nibh vel velit auctor aliquet. Aenean ituin This is Photoshop's version .</p>
                    </div>
                    <div class="service-list">
                        <h4><span><i class="fa fa-usd"></i></span>Charity & Donation</h4>
                        <p>Nibh vel velit auctor aliquet. Aenean ituin This is Photoshop's version .</p>
                    </div>
                    <div class="service-list">
                        <h4><span><i class="fa fa-bell"></i></span>News & Events</h4>
                        <p>Nibh vel velit auctor aliquet. Aenean ituin This is Photoshop's version .</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
