<section class="blog-area section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="section-heading-two">
                    <h2>Our Latest News</h2>
                    <p>Denean sollicitudin. This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit
                        auctor aliquet. Aenean itudin. This is Photoshop's sion of Lorem Ipsum. Proin gravida nibh vel
                        velit.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="blog-slider">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="blog">
                        <figure><img src="{{ asset('website/images/blog/1.jpg') }}" alt=""/></figure>
                        <div class="content">
                            <div class="date"><span><strong>10</strong><em> may</em></span></div>
                            <div class="con mr-b30">
                                <a href="#"><h4>Importance of Prayer</h4></a>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean itudin, lorem quis bibendum
                                    auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit
                                    amet nibh...</p>
                                <a href="#" class="btn6">Read More</a>
                            </div>
                            <ul class="list-inline">
                                <li><i class="fa fa-user"></i>- by Jane Smith</li>
                                <li><i class="fa fa-comment"></i>04 Comm.</li>
                                <li><i class="fa fa-tags"></i>in news, prayer</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="blog">
                        <figure><img src="{{ asset('website/images/blog/2.jpg') }}" alt=""/></figure>
                        <div class="content">
                            <div class="date"><span><strong>10</strong><em> may</em></span></div>
                            <div class="con mr-b30">
                                <a href="#"><h4>Importance of Prayer</h4></a>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean itudin, lorem quis bibendum
                                    auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit
                                    amet nibh...</p>
                                <a href="#" class="btn6">Read More</a>
                            </div>
                            <ul class="list-inline">
                                <li><i class="fa fa-user"></i>- by Jane Smith</li>
                                <li><i class="fa fa-comment"></i>04 Comm.</li>
                                <li><i class="fa fa-tags"></i>in news, prayer</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="blog">
                        <figure><img src="{{ asset('website/images/blog/1.jpg') }}" alt=""/></figure>
                        <div class="content">
                            <div class="date"><span><strong>10</strong><em> may</em></span></div>
                            <div class="con mr-b30">
                                <a href="#"><h4>Importance of Prayer</h4></a>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean itudin, lorem quis bibendum
                                    auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit
                                    amet nibh...</p>
                                <a href="#" class="btn6">Read More</a>
                            </div>
                            <ul class="list-inline">
                                <li><i class="fa fa-user"></i>- by Jane Smith</li>
                                <li><i class="fa fa-comment"></i>04 Comm.</li>
                                <li><i class="fa fa-tags"></i>in news, prayer</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="blog">
                        <figure><img src="{{ asset('website/images/blog/2.jpg') }}" alt=""/></figure>
                        <div class="content">
                            <div class="date"><span><strong>10</strong><em> may</em></span></div>
                            <div class="con mr-b30">
                                <a href="#"><h4>Importance of Prayer</h4></a>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean itudin, lorem quis bibendum
                                    auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit
                                    amet nibh...</p>
                                <a href="#" class="btn6">Read More</a>
                            </div>
                            <ul class="list-inline">
                                <li><i class="fa fa-user"></i>- by Jane Smith</li>
                                <li><i class="fa fa-comment"></i>04 Comm.</li>
                                <li><i class="fa fa-tags"></i>in news, prayer</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
