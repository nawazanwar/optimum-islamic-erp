<?php

return [
    'name' => 'نام',
    'location' => 'مقام',
    'image' => 'تصویر',
    'created_at' => 'تاریخ تخلیق',
    'last_modified' => 'تازہ کاری کی تاریخ',
    'action' => 'عمل',
    'name_ur' => 'اردو میں نام',
    'name_en' => 'انگریزی میں نام',
    'choose_image' => 'اپنی تصویر منتخب کریں',
    'owner' => 'انسٹی ٹیوٹ کا مالک',
    'phone' => 'فون نمبر',
    'cnic' => 'شناختی کارڈ',
    'is_active' => 'ایکٹو ہے',
    'avatar'=>'Avatar',
    'galleries'=>'گیلری / امیجز'
];
