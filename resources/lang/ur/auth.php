<?php

return [
    'login' => [
        'title' => 'Login',
        'info' => 'اپنا سیشن شروع کرنے کے لئے لاگ ان کریں',
        'remember' => 'مجھے پہچانتے ہو',
        'forgot_password' => 'میں اپنا پاسورڈ بھول گیا'
    ],
    'register' => [
        'title' => 'رجسٹر کریں',
        'info' => 'براہ کرم اپنے کردار منتخب کرکے نیا اکاؤنٹ رجسٹر کریں',
    ],
    'or' => 'یا',

    'already_register' => 'اگر آپ نے پہلے ہی اکاؤنٹ رجسٹر کروایا ہے تو براہ کرم لاگ ان کریں',
    'already_not_register' => 'اگر آپ نے پہلے ہی اکاؤنٹ رجسٹر نہیں کیا ہے تو براہ کرم نیا اکاؤنٹ رجسٹر کریں',

    'email_required' => 'First enter your email',
    'password_required' => 'First enter your password'

];
