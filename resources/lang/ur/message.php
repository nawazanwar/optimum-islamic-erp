<?php

return [

    'dou_you_really_wants_to_delete' => 'کیا آپ واقعی اسے حذف کرنا چاہتے ہیں؟',
    'no_record_found' => 'کوئی ریکارڈ نہیں ملا',
    'record_created_success_message' => 'ریکارڈ کامیابی کے ساتھ تشکیل دے دیا گیا',
    'record_updated_success_message' => 'ریکارڈ کامیابی کے ساتھ اپ ڈیٹ ہوا',
    'name_required' => 'برائے کرم نام درج کریں',
    'name_ur_required' => 'برائے کرم اردو میں نام درج کریں',
    'deleted_successfully' => 'ریکارڈ کامیابی کے ساتھ حذف ہوگیا'
];
