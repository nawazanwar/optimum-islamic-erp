<?php
return [
    'home' => 'سرورق',
    'about-us' => 'ہمارے بارے میں جانئے',
    'contact-us' => 'ہم سے رابطہ کریں',
    'e-learning' => 'ای لرننگ',
    'latest-courses' => 'تازہ ترین کورسز',
    'latest-news-and-events' => 'تازہ ترین خبریں'
];
