<?php
return [

    'login' => 'لاگ ان کریں',
    'register' => 'رجسٹر کریں',
    'choose_file' => 'فائل منتخب کریں',

    'login_with_facebook' => 'فیس بک کے ساتھ لاگ ان کریں',
    'login_with_twitter' => 'ٹویٹر کے ساتھ لاگ ان کریں',

    'register_with_facebook' => 'فیس بک کے ساتھ رجسٹر کریں',
    'register_with_twitter' => 'ٹویٹر کے ساتھ رجسٹر کریں',
    'dashboard' => 'ڈیش بورڈ',
    'logout' => 'لاگ آوٹ',
    'create' => 'ایک نئی اندراج درج کریں',
    'search_here' => 'یہاں تلاش کریں',
    'filter' => 'فلٹر کریں',
    'edit' => 'ترمیم',
    'delete' => 'حذف کریں',
    'active' => 'ایکٹو',
    'in_active' => 'غیر ایکٹو',
    'save' => 'محفوظ کریں',
    'show' => 'تفصیل دکھائیں'
];

