<?php

return [
    'welcome_back' => 'خوش آمدید',
    'return-to-home' => 'مین پیج پر واپس جائیں',
    'title' => 'ڈیش بورڈ',
    'institutes' => 'انسٹی ٹیوٹ / مدارس',
    'banners' => 'بینرز / سلائیڈرز',
    'services' => 'خدمات / ماڈیولز',
    'albums' => 'البمز / تصویری گیلری',
    'notifications' => 'اطلاعات',
];
