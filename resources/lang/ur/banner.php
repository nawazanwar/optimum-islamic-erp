<?php
return [
    'title' => 'بینرز',
    'heading' => 'سرخی',
    'heading_ur' => 'اردو میں سرخی',
    'placeholders' => [
        'heading' => 'اپنی سرخی درج کریں',
        'heading_ur' => 'اردو میں اپنی سرخی درج کریں',
    ],
    'choose_bg_image' => 'اپنی پس منظر کی تصویر منتخب کریں',
    'choose_for_image' => 'اپنی اوپری امیج کا انتخاب کریں'
];
