<?php
return [
    'title' => ' البمز',
    'create' => 'نیا البم بنائیں',
    'name' => 'نام',
    'name_ur' => 'اردو میں نام',
    'avatar' => 'البم کے لئے تصویر انتخاب کریں',
    'placeholder' => [
        'name' => 'البم کا نام درج کریں',
        'name_ur' => 'اردو میں البم کا نام درج کریں'
    ],
    'images' => [
        'list' => 'اس البم کی گیلری بنانے کے لئے تمام امیجز کی فہرست',
        'choose_avatar' => 'تصویر کا انتخاب کریں',
        'avatar' => 'گیلری کی تصویر'
    ]
];
